<?php
namespace Cheltar\Module\FileBrowser;
class FileBrowser {
	function renderBrowser() {
		
		$assetApi = new \Cheltar\Api\Asset();
		//$assetApi->bindSystemScriptFile('/file_browser/main.default.js');
		$assetApi->bindSystemDir();
		//$assetApi->bindSystemScriptFile('/file_browser/js/main.default.js');
		
		//$assetApi->bindSystemStyleFile('/bower_components/ckeditor/skins/moono-lisa/editor.css');
		
		//$assetApi->bindSystemFile('/bower_components/ckeditor/skins/moono-lisa/icons.png');
		
		$templateText = \Cheltar\Helper\Template::loadTemplateText("module/elfinder.php");
		
		return \Cheltar\Helper\Template::render([], $templateText);
	}
	
	function uploadProcces() {
		\elFinder::$netDrivers['ftp'] = 'FTP';
			
		function access($attr, $path, $data, $volume, $isDir, $relpath) {
			$basename = basename($path);
			return $basename[0] === '.'                  // if file/folder begins with '.' (dot)
					 && strlen($relpath) !== 1           // but with out volume root
				? !($attr == 'read' || $attr == 'write') // set read+write to false, other (locked+hidden) set to true
				:  null;                                 // else elFinder decide it itself
		}
		$opts = array(
			'debug' => false,
			'roots' => array(
				// Items volume
				array(
					'driver'        => 'LocalFileSystem',           // driver for accessing file system (REQUIRED)
					'path'          => '_upload/',                 // path to files (REQUIRED)
					'URL'           => '/_upload/', // URL to files (REQUIRED)
					'trashHash'     => 't1_Lw',                     // elFinder's hash of trash folder
					'winHashFix'    => DIRECTORY_SEPARATOR !== '/', // to make hash same to Linux one on windows too
					'uploadDeny'    => array('all'),                // All Mimetypes not allowed to upload
					'uploadAllow'   => array('image/x-ms-bmp', 'image/gif', 'image/jpeg', 'image/png', 'image/x-icon', 'text/plain'), // Mimetype `image` and `text/plain` allowed to upload
					'uploadOrder'   => array('deny', 'allow'),      // allowed Mimetype `image` and `text/plain` only
					'accessControl' => 'access'                     // disable and hide dot starting files (OPTIONAL)
				),
				// Trash volume
				array(
					'id'            => '1',
					'driver'        => 'Trash',
					'path'          => '_upload/.trash/',
					'tmbURL'        => '/_upload/.trash/.tmb/',
					'winHashFix'    => DIRECTORY_SEPARATOR !== '/', // to make hash same to Linux one on windows too
					'uploadDeny'    => array('all'),                // Recomend the same settings as the original volume that uses the trash
					'uploadAllow'   => array('image/x-ms-bmp', 'image/gif', 'image/jpeg', 'image/png', 'image/x-icon', 'text/plain'), // Same as above
					'uploadOrder'   => array('deny', 'allow'),      // Same as above
					'accessControl' => 'access',                    // Same as above
				)
			)
		);

		// run elFinder
		$connector = new \elFinderConnector(new \elFinder($opts));
		$connector->run();
	}
}