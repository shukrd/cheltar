<?php

namespace Cheltar\Control;

class SubmitControl extends BaseControl {
	
	protected $type = 'submit';
	protected $className ='btn btn-primary';
			
}