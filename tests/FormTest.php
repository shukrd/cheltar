<?php
use PHPUnit\Framework\TestCase;

class FormTest extends TestCase
{
    public function testForm()
    {
        $object = new \Cheltar\Control\Form('test1', 'test2');
		$this->assertEquals($object->getName(), 'test1');
		$this->assertEquals($object->getAction(), 'test2');
		$this->assertEquals($object->getMethod(), 'POST');
		
		//$f1 = new \Cheltar\Control\TextControl
		$field1 = $this->createMock("\Cheltar\Control\TextControl");
		$field1->method('render')
				->willReturn('f1');
		$field1->method('getVisible')
				->willReturn(true);
		$object->bindControl('name1', 'test1', $field1);
		$object->setTemplateText('echo "123-" . $fields["name1"]["control"] . "-456";');
		$this->assertEquals($object->render(), "123-f1-456");
		
		$object->setTemplateText('?>1234-<?=$fields["name1"]["control"]?>-5678<?');
		$this->assertEquals($object->render(), "1234-f1-5678");
		
		/*$object->setTemplateText('?>1234-<?=$fields["name2"]["control"]?>-5678<?');
		$this->assertEquals($object->render(), "1234--5678");*/
		
    }
	

}
?>