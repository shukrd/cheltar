<?php

namespace Cheltar\ModelConfig;

class BasePageConfig extends BaseConfig  {

	
	function __construct() {
		
		$this->fields['id'] = (new \Cheltar\Field\IntegerField('id', 'Номер'))
				->setKey(true)
				->setDefaultFormControl("\Cheltar\Control\HiddenControl");
		
		$this->fields['title'] = new \Cheltar\Field\StringField('title', "Название");
		
		$this->fields['short_text'] = new \Cheltar\Field\TextField('short_text', "Краткий текст");
		
		$this->fields['text'] = new \Cheltar\Field\TextField('text', "Полный текст");
		
		$this->fields['active'] = new \Cheltar\Field\EnumField('active', 'Включен');
		$this->fields['active']->setOptions([0 => 'нет', 1 => 'да'])
				->setDefaultValue(1);
		
		$this->fields['category_id'] = (new \Cheltar\Field\RelationToOneField('category_id', 'Родительская категория'))
				->setRelatedType('\Cheltar\ModelConfig\Category');
		
		$this->fields['sef'] = (new \Cheltar\Field\SefField('sef', 'ЧПУ'));
		
		$this->fields['create_datetime'] = (new \Cheltar\Field\StringField('create_datetime', "Дата создания"))
				->setInDefaultForm(false)
				->setOnlyRead(true);
		
		$this->fields['modify_datetime'] = (new \Cheltar\Field\StringField('modify_datetime', "Дата последнего изменения"))
				->setInDefaultForm(false)
				->setOnlyRead(true);

		$this->fields['order'] = (new \Cheltar\Field\StringField('order', 'Порядок'))
				->setTableFieldCode('`order`');
				
		$this->fields['view_count'] = (new \Cheltar\Field\IntegerField('view_count', 'Количество просмотров'))
				->setInDefaultForm(false);
				
		$this->modelClassName = "\Cheltar\Model\baseModel";
	}
}
