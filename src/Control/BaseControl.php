<?php

namespace Cheltar\Control;

class BaseControl {
	
	protected $name;
	protected $value;
	protected $defaultValue;
	protected $className = 'form-control';
	protected $type;
	protected $visible = true;
	protected $form;
	protected $needOptions = false; // требуется ли передача информации о доступных вариантах
	protected $options = [];
	/**
	 * 
	 * @param type $name
	 * @param type $value
	 */
	function __construct($name, $value) {
		$this->name = $name;
		$this->value = $value;
	}
	
	function setName($name) {
		$this->name = $name;
		return $this;
	}
	
	function getName() {
		return $this->name;
	}
	
	function setValue($value) {
		$this->value = $value;
		return $this;
	}
	
	function getValue() {
		if (empty($this->value) && isset($this->defaultValue)) {
			return $this->defaultValue;
		} elseif (is_scalar($this->value)) {
			return $this->value;
		}
	}
			
	function setClassName($className) {
		$this->className = $className;
		return $this;
	}
			
	function getClassName() {
		return $this->className;
	}
	
	function setType($type) {
		$this->type = $type;
		return $this;
	}
	
	function getType() {
		return $this->type;
	}
			
	function render() {
		
		return "<input " . $this->getHtmlAttributes() . ">";
	}
	
	function getHtmlAttributes() {
		$arr = [];
		
		if (!empty($this->type)) {
			$arr[] = "type=\"" . $this->type . "\"";
		}

		if (!empty($this->name)) {
			$arr[] = "name=\"" . $this->name . "\"";
		}

		if ((!empty($this->value) || strlen($this->value))) {
			$arr[] = "value=\"" . $this->getValue() . "\"";
		}

		if (!empty($this->className)) {
			$arr[] = "class=\"" . $this->className . "\"";
		}
		
		return implode($arr, " ");
	}
	
	function setVisible($visible) {
		$this->visible = (boolean) $visible;
		return $this;
	}
	
	function getVisible() {
		return $this->visible;
	}
	
	function setForm($form) {
		$this->form = $form;
		return $this;
	}
	
	function getForm() {
		return $this->form;
	}
	
	function isNeedOptions() {
		return $this->needOptions;
	}
	
	function setOptions($options) {
		$this->options = $options;
	}
	
	function setDefaultValue($defaultValue) {
		$this->defaultValue = $defaultValue;
		return $this;
	}
	
	function getDefaultValue() {
		return $this->defaultValue;
	}
	
}