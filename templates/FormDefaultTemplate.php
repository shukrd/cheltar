<form role="form" name="<?=$form["name"]?>" action="<?=$form["action"]?>" method="<?=$form["method"]?>" enctype="<?=$form["enctype"]?>">
<?php
foreach($unvisibleFields as $key=>$field) {
	echo $field . "\n";
}
foreach($fields as $key=>$field) {
?>
	<div class="form-group">
	  <label><?=$field["caption"]?></label>
	  <?=$field["control"]?>
	</div>
<?php
}
?>

</form>
  <script> 
document.addEventListener('DOMContentLoaded',
	function () {
		CKEDITOR.replaceAll('format_text');
		CKEDITOR.config.filebrowserBrowseUrl = '/adminajax/filebrowser/';
	}
);
  </script>