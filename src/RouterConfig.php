<?php

namespace Cheltar;
use \Cheltar\Model\RouteNode;

class RouterConfig {
	protected $baseRouteNode;
	public __construct() {
		$this->baseRouteNode = $this->makeRouteNodeObject('');
		
		$adminRouteNode = $this->makeRouteNodeObject('admin', ['']);
		$this->baseRouteNode->addChildNode($adminRouteNode);
		
	}
	
	public makeRouteNodeObject($segment, $callback = null, $argumentList = []) {
		return new RouteNode($segment, $callback, $argumentList);
	}
}