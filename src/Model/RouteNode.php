<?php
namespace Cheltar\Model;

class RouteNode implements RouteNodeInterface {
	
	protected $segment;
	protected $callback = null;
	protected $argumentList = [];
	protected $parentNode = null;
	protected $childrenNodeList = [];
	
	function __construct($segment, $callback = null, $argumentList = []) {
		$this->segment = $segment;
		$this->callback = $callback;
		$this->argumentList = $argumentList;
	}
	
	function setParentNode(RouteNodeInterface $parentNode): RouteNodeInterface {
		$this->parentNode = $parentNode;
		return $this;
	}
	
	function getParentNode(): RouteNodeInterface {
		return $this->parentNode;
	}
	
	function addChildNode(RouteNodeInterface $childNode): RouteNodeInterface {
		$this->childrenNodeList[$childNode->getSegment()] = $childNode;
		$childNode->setParentNode($this);
		return $this;
	}
	
	function getChildNode($segment): RouteNodeInterface {
		if (isset($this->childrenNodeList[$segment])) {
			return $this->childrenNodeList[$segment];
		} else {
			return null;
		}
	}
	
	function getSegment(): string {
		return $this->segment;
	}
	
	function run ($segmentList) {
		$segment = array_shift($segmentList);
		if (empty($this->childrenNodeList[$segment])) {
			if (!empty($this->callback)) {
				return call_user_func_array($this->callback, $this->argumentList);
			}
		} else {
			return $this->childrenNodeList[$segment]->run($segmentList);
		}
		return false;
	}
}