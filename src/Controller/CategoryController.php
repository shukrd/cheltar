<?php
namespace Cheltar\Controller;

use Cheltar\Helper\Factory;
use \Cheltar\App\app;

class CategoryController extends BaseController {
	
	function viewAction($object) {
		$apiClassName = $this->getConfigObject()->getApiClassName();
		$api = new $apiClassName($this->getConfigObject());
		
		$factory = new \Cheltar\Model\baseModelFactory($this->getConfigObject());
		$postConfig = new \Cheltar\ModelConfig\Post();
		$postFactory = new \Cheltar\Model\baseModelFactory($postConfig);
		if (!is_object($object)) {
			$object = $factory->getModelByFields(['id' => $object]);			
		}
		$cats = $factory->getModelsArrayByFields(['category_id' => $object->id, 'active' => 1], '`order` ASC, `create_datetime` ASC');
		$categories = [];
		foreach($cats as $cat) {
			$categories[] = [
				'title' => $cat->title,
				'short_text' => $cat->short_text,
				'link' => $cat->getUrl()
			];
		}
		$params = [
			"text" => $object->text,
			"list" => $categories
		];
		$templateText = \Cheltar\Helper\Template::loadTemplateText("list.php");
		$result = \Cheltar\Helper\Template::render($params, $templateText);
		
		
		$currenPage = isset($_GET['p'])?(int)$_GET['p']:0;
		$countOnPage = 10;
		
		$postModelList = new \Cheltar\Model\baseModelsList($postConfig);
		$postModelList
				->setFilters(['category_id' => $object->id, 'active' => 1])
				->setOrder('`order` ASC, create_datetime DESC')
				->setLimit($countOnPage)
				->setOffset($countOnPage * $currenPage);
		//$qb = $postModelList->getQueryBuilder();

		if (!empty($_GET['y'])) {
			if (!empty($_GET['m']) && !empty($_GET['d'])) {
				$date = $_GET['y'] . "-" . $_GET['m'] . "-" . str_pad($_GET['d'], 2, '0', STR_PAD_LEFT) . "%";
			} elseif (!empty($_GET['m'])) {
				$date = $_GET['y'] . "-" . $_GET['m'] . "%";
			} else {
				$date = $_GET['y'] . "%";
			}
			$postModelList->addCondition('LIKE', 'create_datetime', addslashes($date));
		}

		
		$psts = $postModelList->getModelsArray();


		
		$posts = [];
		foreach($psts as $post) {
			$posts[] = [
				'title' => $post->title,
				'short_text' => $post->short_text,
				'text' => $post->text,
				'create_datetime' => $post->create_datetime,
				'link' => $post->getUrl(),
				'list_image' => $post->list_image
			];
		}
		
		$paginatorObject = (new \Cheltar\Control\Paginator())
				->setTotalCount($postModelList->getTotalCount())
				->setCountOnPage($countOnPage)
				->setUrl($_SERVER['REQUEST_URI']);

		$paginator = $paginatorObject->render();
		
		//print_r($post);
		$params = [
			"text" => '',
			"list" => $posts,
			"paginator" => $paginator
		];
		//$templateText = \Cheltar\Helper\Template::loadTemplateText("list.php");
		if (!empty($_SESSION['user'])) {
			app::getToolsPanel()->addTool(['link' => $api->getUrl($object, 'update', 'admin'), 'text' => 'Редактировать']);
			
			if ($this->configObject->getFieldParams('category_id')) {
				app::getToolsPanel()->addTool(['link' => $api->getUrl($object, 'addtosamecategory', 'admin'), 'text' => 'Добавить в эту же категорию']);
				app::getToolsPanel()->addTool(['link' => $api->getUrl($object, 'addsubcategory', 'admin'), 'text' => 'Добавить подкатегорию']);
			}
		}
		
		$object->view_count++;
		$object->save();
		
		$result .= \Cheltar\Helper\Template::render($params, $templateText);
		return $result;
	}
	
	function addsubcategoryAction($key) {
		Header('X-XSS-Protection: 0');
		$factory = new \Cheltar\Model\baseModelFactory($this->getConfigObject());
		$parentObject = $factory->getModelByFields(['id' => $key]);
		$object = new \Cheltar\Model\baseModel($this->configObject);
		$object->category_id = $parentObject->id;
		if (isset($_POST[$this->configObject->getCode()])) {
			$object->setData($_POST[$this->configObject->getCode()]);
			$object->save();
			if (!empty($object->getFirstKey())) {
				$url = \Cheltar\Helper\Factory::getInstance()->getModelApi($this->configObject)->getUrl($object, 'update', 'admin');
				header("location:" . $url);
			}
		}
		
		$form = new \Cheltar\Control\ModelForm($object);
		
		return $form->render();
	}
}
