<?php

namespace Cheltar\Control;

class Form {
	protected $name;
	protected $action;
	protected $method;
	
	protected $fields = [];

	protected $templateText = '';
	
	protected $enctype = '';
	
	function __construct($name, $action = '', $method = 'POST') {
		$this->name = $name;
		$this->action = $action;
		$this->method = $method;
	}
	
	function bindControl($code, $caption, $control) {
		$this->fields[$code]['control'] = $control;
		$this->fields[$code]['caption'] = $caption;
		$this->fields[$code]['visible'] = $control->getVisible();
		$control->setForm($this);
		
		if ($control->getType() == 'file') {
			$this->setEnctype('multipart/form-data');
		}
		
		return $this;
	}
	
	function unBindControl($code) {
		unset($this->fields[$code]);
		return $this;
	}
	
	function setTemplateText($templateText) {
		$this->templateText = $templateText;
		return $this;
	}
	
	function getTemplateText() {
		return $this->templateText;
	}
	
	function render() {
		$fields = [];
		$unvisibleFields = [];
		foreach ($this->fields as $key=>$field) {
			
			if ($field['visible']) {
				$fields[$key]['control'] = $field['control']->render();
				$fields[$key]['caption'] = $field['caption'];
			} else {
				$unvisibleFields[$key] = $field['control']->render();
			}
			
		}
		
		$form = [
			'action' => $this->getAction(),
			'name' => $this->getName(),
			'method' => $this->getMethod(),
			'enctype' => $this->getEnctype()
		];
		if (empty($this->templateText)) {
			$this->templateText = \Cheltar\Helper\Template::loadTemplateText("FormDefaultTemplate.php");
		}

		$result = \Cheltar\Helper\Template::render(['fields' => $fields, 'unvisibleFields' => $unvisibleFields, 'form' => $form], $this->templateText);

		return $result;
	}
	
	function setName($name) {
		$this->name = $name;
		return $this;
	}
	
	function getName() {
		return $this->name;
	}
	
	function setAction($action) {
		$this->action = $action;
		return $this;
	}
	
	function getAction() {
		return $this->action;
	}
	
	function setMethod($method) {
		$this->method = $method;
		return $this;
	}
	
	function getMethod() {
		return $this->method;
	}
	
	function setEnctype($enctype) {
		$this->enctype = $enctype;
	}
	
	function getEnctype() {
		return $this->enctype;
	}
}