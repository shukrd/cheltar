<?php
namespace Cheltar\Controller;
use \Cheltar\Helper\Factory;
use \Cheltar\App\app;
class MainAdminController {
	
	public function main($arr) {
		
		$h1 = $title = "Управление сайтом";
		
		if (empty($_SESSION['user'])) {
			header("location:/login/");
		}
		$templateText = \Cheltar\Helper\Template::loadTemplateText("admin/index.php");
		if (empty($arr[1])) {
			$code = 'category';
		} else {
			$code = $arr[1];
		}
		if (in_array($code, ['post', 'category', 'menu', 'textblocks'])) {

			if ($code == 'post') {
				$configObject = new \Cheltar\ModelConfig\Post();
			} elseif ($code == 'category') {
				$configObject = new \Cheltar\ModelConfig\Category();
			} elseif ($code == 'menu') {
				$configObject = new \Cheltar\ModelConfig\Menu();
			} elseif ($code == 'textblocks') {
				$configObject = new \Cheltar\ModelConfig\TextBlock();
			}
			
			//$controller = new \Cheltar\Controller\BaseController($configObject);
			$controller = Factory::getInstance()->getModelController($configObject);
			$content = '';
			if (!isset($arr[2])) {
				$content = $controller->indexAction([]);
			} elseif ($arr[2] == 'create') {
				$content = $controller->createAction();
			} elseif (preg_match ("/^\d*$/", $arr[2], $match)) {
				$content = $controller->viewAction($match[0]);
			} elseif (!empty($arr[3])) {
				if (preg_match ("/^\d*$/", $arr[3], $match)) {
					$id = $match[0];
					if ($arr[2] == 'update') {
						$content = $controller->updateAction($id);
					} elseif ($arr[2] == 'delete') {
						$content = $controller->deleteAction($id);
					} elseif ($arr[2] == 'addtosamecategory') {
						$content = $controller->addtosamecategoryAction($id);
					} elseif ($arr[2] == 'addsubcategory') {
						$content = $controller->addsubcategoryAction($id);
					}
				}
			}
		}
		
		if (empty($breadCrumbs)) {
			$bc = '';
		} else {
			$bc = $breadCrumbs->render();
		}
		$tools = app::getToolsPanel()->render();
		
		$options = [
			'title' => $title,
			'h1' => $h1,
			//'description' => $description,
			'content' => $content,
			'bc' => $bc, 
			'tools' => $tools, 
		];
			
		echo \Cheltar\Helper\Template::render($options, $templateText);
	}
}
