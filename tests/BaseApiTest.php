<?php
use PHPUnit\Framework\TestCase;
use Cheltar\Api\BaseApi;
class BaseApiTest extends TestCase
{
    public function testGetUrlSegments()
    {
		$object = BaseApi::getInstance();
		$this->assertEquals($object->getUrlSegments(''), []);
		$this->assertEquals($object->getUrlSegments('/'), []);
		$this->assertEquals($object->getUrlSegments('/test'), ['test']);
		$this->assertEquals($object->getUrlSegments('/test/'), ['test']);
		$this->assertEquals($object->getUrlSegments('/test?ab=cd'), ['test']);
		$this->assertEquals($object->getUrlSegments('/test1/test2/?a=b&c=d'), ['test1', 'test2']);
	
    }
	

}
?>