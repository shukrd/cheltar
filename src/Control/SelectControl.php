<?php

namespace Cheltar\Control;

class SelectControl extends BaseControl {
	protected $needOptions = true;
	
	function render() {
		
		$arr = [];
		
		if (!empty($this->name)) {
			$arr[] = "name=\"" . $this->name . "\"";
		}

		if (!empty($this->className)) {
			$arr[] = "class=\"" . $this->className . "\"";
		}
		
		return "<select " . implode($arr, " ") . ">" . $this->renderOptions() . "</select>";
	}
	
	function renderOptions() {
		$str = "";
		$selectValue = $this->getValue();
			
		foreach((array) $this->options as $value => $text) {
			$str .= "<option value='" . $value . "' " . (((string)$value === (string)$selectValue)?"selected":"") . ">" . $text . "</option>";
		}
		return $str;
	}
}