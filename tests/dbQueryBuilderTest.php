<?php
use PHPUnit\Framework\TestCase;

class dbQueryBuilder extends TestCase
{
    public function testGetQuery()
    {
        $obj = new \Cheltar\Db\queryBuilder();
		$obj->insertQuery("testTable");
		$this->assertEquals($obj->getQuery(), 'INSERT INTO `testTable`');
    }
}
?>