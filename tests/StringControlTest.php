<?php
use PHPUnit\Framework\TestCase;

class StringControlTest extends TestCase
{
    public function testControl()
    {
        $object = new \Cheltar\Control\StringControl('test1', 'test2');
	
		$this->assertEquals($object->render(), '<input type="text" name="test1" value="test2" class="form-control">');
	
    }
	

}
?>