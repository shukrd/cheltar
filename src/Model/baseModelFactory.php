<?php

namespace Cheltar\Model;

class baseModelFactory {
	
	/**
	 *
	 * @var \Cheltar\modelConfig\base
	 */
	protected $modelConfig;
	
	function __construct($modelConfig) {
		$this->modelConfig = $modelConfig;
	}
	
	function prepareQueryBuilder($row) {
		$qb = $this->getQueryBuilder();
		$qb->setFrom($this->modelConfig->getTableName());
		foreach ((array)$row as $key => $value) {
			$qb->addAndWhere($key . " = '" . addslashes($value) . "'");
		}
		return $qb;
	}
	
	function getModelByFields($row) {
		
		global $db;
		
		$qb = $this->prepareQueryBuilder($row);
		$res = $db->getPDO()->query($qb->getQuery());
		if ($res) {
			$row = $res->fetch(\PDO::FETCH_ASSOC);
		}
		
		if (empty($row)) {
			return false;
		}
		
		$object = $this->getModelObject();
		$object->setData($row);
		return $object;
		/*print_r($qb->getQuery());
		print_r($row);*/
	}
	
	function getModelsArrayByFields($row, $order = null) {
		global $db;
		
		$qb = $this->prepareQueryBuilder($row);
		if (!empty($order)) {
			$qb->addOrderBy($order);
		}
		
		$result = [];
		//print_r($qb->getQuery());
		$res = $db->getPDO()->query($qb->getQuery(), \PDO::FETCH_ASSOC);
		if (empty($res)) {
			return [];
		}
		
		foreach ($res as $row) {
			$object = $this->getModelObject();
			$object->setData($row);
			$result[] = $object;
		}
		return $result;
	}
	
	function getQueryBuilder() {
		return new \Cheltar\Db\queryBuilder();
	}
	
	function getModelObject() {
		$className = $this->modelConfig->getModelClassName();
		return new $className($this->modelConfig);
	}
	
	function getModelApi() {
		
	}

}
