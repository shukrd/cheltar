<?php

namespace Cheltar\Control;

class HiddenControl extends BaseControl {
	
	protected $type = 'hidden';
	protected $visible = false;
}