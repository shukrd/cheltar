<?php

namespace Cheltar\App;

class app {

	protected static $config = [];
	protected static $configList = [];
	protected static $pdo;
	protected static $breadCrumbs;
	protected static $toolsPanel;
	protected static $container;


	static function getConfigObject($configClassName) {
		if (!empty(self::$configList[$configClassName])) {
			return self::$configList[$configClassName];
		}

		if (class_exists($configClassName)) {
			return self::$configList[$configClassName] = new $configClassName();
		} else {
			return false;
		}
	}

	static function setContainer($container) {
		self::$container = $container;
	}

	static function getContainer(){
		return self::$container;
	}
	
	static function setConfig($config = []) {
		self::$config = $config;
	}

	static function getPdo() {
		if (empty(self::$pdo)) {
			self::$pdo = self::$container->db->getPdo();
		}
		return self::$pdo;
	}

	static function getModel($configClassName) {
		if (class_exists($configClassName)) {
			
		} else {
			return false;
		}
	}
	
	static function getBreadCrumbs() {
		if (empty(self::$breadCrumbs)) {
			self::$breadCrumbs = new \Cheltar\Control\BreadCrumbs();
		}
		return self::$breadCrumbs;
	}

	static function getToolsPanel() {
		if (empty(self::$toolsPanel)) {
			self::$toolsPanel = new \Cheltar\Control\ToolsPanel();
		}
		return self::$toolsPanel;		
	}
	
	public static function run($container) {
		session_start();
		static::setContainer($container);
		
		echo $container->mainController->main($_SERVER['REQUEST_URI']);
		
	}
}
