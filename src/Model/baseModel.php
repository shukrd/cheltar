<?php

namespace Cheltar\Model;

class baseModel {

	protected $_data = [];
	/**
	 *
	 * @var \Cheltar\modelConfig\base
	 */
	protected $_modelConfig;

	protected $modelApi;
	
	function __construct($modelConfig) {
		$this->_modelConfig = $modelConfig;
	}

	function __get($name) {
		if (isset($this->_data[$name]) && $field = $this->_modelConfig->getFieldParams($name)) {
			return $field->getter($this->_data, $this->_modelConfig);
		} else {
			return null;
		}
	}
	
	function __set($name, $value) {
		if ($this->_modelConfig->getFieldParams($name)) {
			$this->_data[$name] = $value;
		}
	}
	
	function getModelConfig() {
		return $this->_modelConfig;
	}
	
	function setData($data) {
		foreach($data as $key => $value) {
			$this->$key = $value;
		}
	}
	
	function getQueryBuilder() {
		return new \Cheltar\Db\queryBuilder();
	}
	
	function save() {
		global $db;
		$fields = $this->_modelConfig->getFieldCodesList();
		
		$data = [];
		
		foreach ($fields as $code) {
			$field = $this->_modelConfig->getFieldParams($code);
			if ($field->isOnlyRead()) {
				continue;
			}
			$data[$code] = $field->prepareBeforeSave($this->_data, $this->_modelConfig);
		}
		
		
		$qb = $this->getQueryBuilder();
		$fields = \array_keys($data);
		$values = \array_values($data);
		
		$qb->insertQuery($this->_modelConfig->getTableName())
				->setFields($fields)
				->setValues($values)
				->setOnDuplicateUpdate($data);
		
		$query = $qb->getQuery();
		$res = $db->getPDO()->query($query);

		
		$id = $db->getPDO()->lastInsertId();
		
		if (!$this->areKeyFieldsFilled()) {
			$this->setFirstKey($id);
		}
	}
	
	function areKeyFieldsFilled() {//print_r($this->f1);
		foreach ($this->getModelConfig()->getKeyFields() as $code) {
			
			if (empty($this->_data[$code])) {
				return false;
			}
		}
		return true;
	}
	
	function setFirstKey($id) {
		$keys = $this->getModelConfig()->getKeyFields();
		$key = reset($keys);
		if (!empty($key)) {
			$this->$key = $id;
		}
	}
	
	function getFirstKey() {
		$keys = $this->getModelConfig()->getKeyFields();
		$key = reset($keys);
		return $this->$key;
	}

	function getUrl($mode = 'client') {
		return $this->getModelApi()->getUrl($this, 'view', $mode);
	}
	
	function getModelApi() {
		if (empty($this->modelApi)) {
			$this->modelApi = \Cheltar\Helper\Factory::getInstance()->getModelApi($this->getModelConfig());
		}
		return $this->modelApi;
	}
	
	function getParent() {
		//todo переделать на тип поля
		$object = false;
		if ($this->category_id) {
			$factory = new \Cheltar\Model\baseModelFactory(new \Cheltar\ModelConfig\Category());
			$object = $factory->getModelByFields(['id' => $this->category_id]);
		}
		return $object;
	}
}
