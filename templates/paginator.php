						  <div class="dataTables_paginate paging_simple_numbers" id="example2_paginate">
							  <ul class="pagination">
<?php
	foreach ($paginator['list'] as $k => $page) {
?>
								<li class="paginate_button <?=(empty($page['active'])?"":"active")?>">
									<a href="<?=$page['url']?>" aria-controls="example2" data-dt-idx="<?=$page['text']?>" tabindex="0"><?=$page['text']?></a>
								</li>
<?php
	}
?>
								  <!--<li class="paginate_button previous disabled" id="example2_previous"><a href="#" aria-controls="example2" data-dt-idx="0" tabindex="0">Previous</a></li>-->
								  <!--<li class="paginate_button active"><a href="#" aria-controls="example2" data-dt-idx="1" tabindex="0">1</a></li>
								  <li class="paginate_button "><a href="#" aria-controls="example2" data-dt-idx="2" tabindex="0">2</a></li>
								  <li class="paginate_button "><a href="#" aria-controls="example2" data-dt-idx="3" tabindex="0">3</a></li>
								  <li class="paginate_button "><a href="#" aria-controls="example2" data-dt-idx="4" tabindex="0">4</a></li>
								  <li class="paginate_button "><a href="#" aria-controls="example2" data-dt-idx="5" tabindex="0">5</a></li>
								  <li class="paginate_button "><a href="#" aria-controls="example2" data-dt-idx="6" tabindex="0">6</a></li>
								  <li class="paginate_button next" id="example2_next"><a href="#" aria-controls="example2" data-dt-idx="7" tabindex="0">Next</a></li>-->
							  </ul>
						  </div>