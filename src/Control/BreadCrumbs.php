<?php
namespace Cheltar\Control;

class BreadCrumbs {
	protected $links = [];
	protected $templateText = '';
			
	function setLinks(array $links) {
		$this->links = $links;
		return $this;
	}
	
	function getLinks() {
		return $this->links;
	}
	
	function addLink($text, $link) {
		$this->links[$text] = $link;
		return $this;
	}
	
	function getTemplateText() {
		if (empty($this->templateText)) {
			$this->templateText = \Cheltar\Helper\Template::loadTemplateText("paginator.php");
		}
		
		return $this->templateText;
	}
	
	function setTemplateText($templateText) {
		$this->templateText = $templateText;
		return $this;
	}
	
	function render() {
		//$paginator = $this->getPaginatorArray();
		$templateText = \Cheltar\Helper\Template::loadTemplateText("breadcrumbs.php");
		return \Cheltar\Helper\Template::render(['links' => $this->links], $templateText);
	}
}
