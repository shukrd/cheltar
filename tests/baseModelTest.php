<?php
use PHPUnit\Framework\TestCase;

class baseModelTest extends TestCase
{
    public function testAreKeyFieldsFilled()
    {
		$field = new \Cheltar\Field\BaseField('f1');
		/*$config->method('getter')
			->willReturn('');*/
		
		
        $config = $this->getMockBuilder("\Cheltar\ModelConfig\BaseConfig")
                         ->setMethods(['getKeyFields', 'getFieldParams'])
                         ->getMock();
		$config->method('getKeyFields')
				->willReturn(['f1']);
				
		$config->method('getFieldParams')
				->willReturn($field);
        
		$obj = new \Cheltar\Model\baseModel($config);
		$this->assertFalse($obj->areKeyFieldsFilled());
		
		$obj->f1 = '123';
		$this->assertEquals($obj->f1, '123');
		$this->assertTrue($obj->areKeyFieldsFilled());
		
		$obj->f1 = '';
		$this->assertFalse($obj->areKeyFieldsFilled());
    }
	

}
?>