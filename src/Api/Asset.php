<?php
namespace Cheltar\Api;

class Asset  {
	protected $files = [];
	protected $systemAssetDir;
	protected $projectAssetDir;
	
	static protected $sourses = [];
	static protected $styleFiles = [];
	static protected $scriptFiles = [];
	
	static protected $documentRoot = null;
	
	function bindSystemFile($source) {
		//$sourceFile = $this->getSystemAssetDir() . $source;
		return $this->bindFile($this->getSystemAssetDir(), $source);
	}
	
	function bindSystemDir() {
		$sourceDir = $this->getSystemAssetDir();
		$this->bindDir($sourceDir);
	}
	
	function bindDir($sourceDir) {
		$targetDir = $this->getDirNameInAsset($sourceDir);
		$this->copyDir($sourceDir, $targetDir);
	}
	
	function copyDir($sourceDir, $targetDir) {
		if ( is_dir( $sourceDir ) ) {
			$this->_mkDir($targetDir, 0777, true);
			
			$d = dir( $sourceDir );
			while ( false !== ( $entry = $d->read() ) ) {
				if ( $entry != '.' && $entry != '..' ) 
					$this->copyDir($sourceDir . $this->_getDS() . $entry, $targetDir  . $this->_getDS() . $entry);
			}
			$d->close();
		}
		else {
			$this->_copy( $sourceDir, $targetDir);
		}
	}
			
	function bindFile($dir, $source) {
		$pathInfo = pathinfo($source);
		$sourceDir = $pathInfo['dirname'];
		$fileName = $pathInfo['basename'];
		
		if ($sourceDir == '.') {
			$sourceDir = '';
		}
		$targetDir = $this->preparePath($this->getDirNameInAsset($dir) . $sourceDir);
		$targetFile = $this->preparePath($targetDir . $this->_getDS() . $fileName);
		$sourceFile = $this->preparePath($dir . $this->_getDS() . $source);
		$documentRoot = $this->getDocumentRoot();
		if (!$this->_fileExists($targetDir)) {
			$this->_mkDir($targetDir, 0777, true);
		}
		$this->_copy($sourceFile, $targetFile);
		/*echo "<p>" . $sourceFile . " - " . date("Y-m-d H:i:s", filemtime($sourceFile)) . " - " . date("Y-m-d H:i:s", filectime($sourceFile));
		echo "<p>" . $targetFile . " - " . date("Y-m-d H:i:s", filemtime($targetFile)) . " - " . date("Y-m-d H:i:s", filectime($targetFile));
		echo "<p>" . $targetDir . " - " . date("Y-m-d H:i:s", filemtime($targetDir)) . " - " . date("Y-m-d H:i:s", filectime($targetDir));
		echo "<hr>";*/
		
		$src = "/" . str_replace($documentRoot, '', $targetFile);
		$src = $this->preparePath($src, '/');
		self::$sourses[$targetFile] = $src;
		return $src;
	}
	
	function getDocumentRoot() {

		if (!isset(self::$documentRoot)) {
			self::$documentRoot = $this->preparePath($this->_getDocumentRoot());		
		}

		return self::$documentRoot;
	}
	
	function getSourseFile($targetFile) {
		if (isset(self::$sourses[$targetFile])) {
			return self::$sourses[$targetFile];
		} else {
			return self::$sourses;
		}
	}
	
	function bindSystemScriptFile($source) {
		$src = $this->bindSystemFile($source);
		self::$scriptFiles[$source] = $src;
		return $src;
	}

	function bindSystemStyleFile($source) {
		$src = $this->bindSystemFile($source);
		self::$styleFiles[$source] = $src;
		return $src;
	}
			
	function getSystemAssetDir() {
		if (empty($this->systemAssetDir)) {
			$this->systemAssetDir = $this->_getDir() . $this->_getDS(). ".." . $this->_getDS() . ".." . $this->_getDS() . "assets";
		}
		return $this->systemAssetDir;
	}
	
	function _getDir() {
		return __DIR__;
	}
	
	function getProjectAssetDir() {
		if (empty($this->projectAssetDir)) {
			$this->projectAssetDir = $this->_getDocumentRoot() . $this->_getDS() . "assets" ;
		}
		return $this->projectAssetDir;
	}
	
	function _getDocumentRoot() {
		return $_SERVER['DOCUMENT_ROOT'];
	}
	
	function _getDS() {
		return DIRECTORY_SEPARATOR;
	}
	
	function getDirNameInAsset($dir) {
		$result = $this->getProjectAssetDir() . $this->getMd5Dir($dir);
		return $result;
	}
	
	function getMd5Dir($dir) {
		$md5 = md5($dir);
		$dir1 = substr($md5, 0, 2);
		$dir2 = substr($md5, 2, 2);
		$dir3 = substr($md5, 4);
		return $this->_getDS() . $dir1 . $this->_getDS() . $dir2 . $this->_getDS() . $dir3;
	}
	
	function _fileExists($filename) {
		return file_exists($filename);
	}
	
	function _fileMTime($filename) {
		return filemtime($filename);
	}
	
	function _time() {
		return time();
	}
	
	function _copy($source, $dest) {
		return copy($source, $dest);
	}
	
	function _mkDir($dir, $mode = 0777, $recursive = false) {
		if (!is_dir($dir)) {
			mkdir($dir, $mode, $recursive);
		}
	}
	
	
	function renderStyleLinks() {
		$strArr = [];
		foreach(self::$styleFiles as $key=>$value) {
			$strArr[] = '<link rel="stylesheet" href="' . $value . '">';
		}
		return implode("\n", $strArr);
	}
	
	function renderScriptLinks() {
		$strArr = [];
		foreach(self::$scriptFiles as $key=>$value) {
			$strArr[] = '<script src="' . $value . '"></script>';
		}
		return implode("\n", $strArr);
	}
	
	function renderSrc() {
		$strArr = [];
		
		foreach(self::$sourses as $key=>$value) {
			$type = pathinfo($key, PATHINFO_EXTENSION);
			switch ($type) {
				case 'css':
					$strArr[] = '<link rel="stylesheet" href="' . $value . '">';
					break;
				case 'js':
					$strArr[] = '<script src="' . $value . '"></script>';
					break;
					
			}
		}

		return implode("\n", $strArr);
	}
	
	function preparePath($path, $separator = null) {
		if (empty($separator )) {
			$separator = $this->_getDS();
		}
		return preg_replace("/[\\/\\\\]+/", $separator, $path);
	}
	
	function getSrc($filePath) {
		$src = str_replace($this->getDocumentRoot(), '', $this->preparePath($filePath));
		$src = $this->preparePath("/" . $src, '/');
		return $src;
	}
}