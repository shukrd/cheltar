<?php

namespace Cheltar\ModelConfig;

class BaseConfig {

	protected $code = 'base';
	protected $fields = []; // список полей
	
	// список символьных кодов полей
	protected $fieldsList = null;

	protected $_tableName;
	
	protected $keyFields = null;
	
	protected $modelClassName;
	
	protected $apiClassName = "\Cheltar\Api\ModelApi";

	protected $controllerClassName = "\Cheltar\Controller\BaseController";
			
	function getFieldCodesList() {
		if (!isset($this->fieldsList)) {
			$this->fieldsList = array_keys($this->fields);
		}

		return $this->fieldsList;
	}

	function getFieldParams($fieldName) {
		if (!isset($this->fields[$fieldName])) {
			return false;
		}
		return $this->fields[$fieldName];
	}
	
	function getTableName() {
		return $this->_tableName;
	}
	
	function getCode() {
		return $this->code;
	}

	function getKeyFields() {
		if (!isset($this->keyFields)) {
			$this->keyFields = [];
			foreach ($this->fields as $key => $field) {
				if ($field->isKeyField()) {
					$this->keyFields[] = $key;
				}
			}
		}
		return $this->keyFields;
	}
	
	function getModelClassName() {
		return $this->modelClassName;
	}
	
	function getApiClassName() {
		return $this->apiClassName;
	}
	
	function getControllerClassName() {
		return $this->controllerClassName;
	}
	
}
