<?php
use PHPUnit\Framework\TestCase;

class ServiceContainerTest extends TestCase
{
    public function testSetElement()
    {
        $obj = new \Cheltar\Service\Container();
		$obj->setElement('test1', 'abc123');
		
		$this->assertEquals('abc123', $obj->test1);
		
		$obj->setElement('test1', 'abc');
		$this->assertEquals('abc', $obj->test1);
    }
	
	public function testSetConctructor() {
		
		$obj = new \Cheltar\Service\Container();
		$obj->setConstructor('test', '\Cheltar\Model\RouteNode', ['123']);
		
		$el = $obj->test;
		
		$this->assertTrue($el instanceof \Cheltar\Model\RouteNode);
		$this->assertEquals('123', $el->getSegment());
	}
}
?>