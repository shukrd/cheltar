<?php

namespace Cheltar\Helper;


class Template {
	protected static $moduleDir;
	protected static $templateDir;
	
	static function render($params, $template) {
		
		foreach ((array)$params as $key => $value) {
			$$key = $value;
		}
		
		ob_start();

		eval($template);

		$result = ob_get_clean();
		
		return $result;
	}
	
	static function getModuleTemplatesDir() {
		if (empty(self::$templateDir)) {
			self::$templateDir = self::getModelDir() . "templates" . DIRECTORY_SEPARATOR;
		}
		return self::$templateDir;
	}
	
	static function getModelDir() {
		if (empty(self::$moduleDir)) {
			self::$moduleDir = self::_getModuleDir(__DIR__);
		}
		return self::$moduleDir;
	}

	static function _getModuleDir($dir) {
		
		return $dir . DIRECTORY_SEPARATOR . ".." . DIRECTORY_SEPARATOR . ".." . DIRECTORY_SEPARATOR;
	}

	static function loadTemplateText($path) {
		$result = file_get_contents(self::getModuleTemplatesDir() . $path);
		return "?>" . $result;
	}
	
	static function loadProjectTemplateText($path) {
		$fullpath = $_SERVER["DOCUMENT_ROOT"] . DIRECTORY_SEPARATOR . $path;
		$result = file_get_contents($fullpath);
		return "?>" . $result;
	}
	
	static function renderMenu($menuCode, $ulClass = '', $liClass = '', $tempatePath = '') {
		if (empty($tempatePath)) {
			$templateText = self::loadTemplateText('menu.php');
		} else {
			$templateText = self::loadProjectTemplateText($tempatePath);
		}
		$config = new \Cheltar\ModelConfig\Menu();
		$apiClassName = $config->getApiClassName();
		$api = new $apiClassName($config);
		$factory = new \Cheltar\Model\baseModelFactory($config);
		$menuObject = $factory->getModelByFields(['code' => $menuCode, 'active' => 1]);
		if (empty($menuObject)) {
			return false;
		}
		$res = $factory->getModelsArrayByFields(['parent_id' => $menuObject->id, 'active' => 1], '`order` ASC');
		$items = [];
		foreach ($res as $object) {
			$item = [
				'id' => $object->id,
				'text' => $object->title,
				'url' => $object->url,
				'image' => $object->image,
				'target' => $object->target
			];
			
			if (!empty($_SESSION['user'])) {
				$item['update'] = $api->getUrl($object, 'update', 'admin');
			}
			
			$items[] = $item;
		}
		
		$params = [
			'ulClass' => $ulClass,
			'liClass' => $liClass,
			'items' => $items
		];
		return \Cheltar\Helper\Template::render($params, $templateText);
	}

	static function renderTextBlock($textBlockCode) {
		$factory = new \Cheltar\Model\baseModelFactory(new \Cheltar\ModelConfig\TextBlock());
		$row = $factory->getModelByFields(['code' => $textBlockCode, 'active' => 1]);
		
		if (empty($row)) {
			return '';
		}
		
		$text = strlen($row->text) > 0 ? $row->text : $row->formattext;
		
		return $text;
		
	}
}