<?
namespace Cheltar\Service;

class uploadFile {
	function upload($arr, $name) {
			$assetApi = new \Cheltar\Api\Asset();
			$dir = "/_upload/" . $assetApi->getMd5Dir(time()) ."/";
			$path = $_SERVER['DOCUMENT_ROOT'] . $dir;
			if (is_array($arr["name"])) {
				$fileName = $arr["name"][$name];
				$tmp_name = $arr["tmp_name"][$name];
			} else {
				$fileName = $arr["name"];
				$tmp_name = $arr["tmp_name"];
			}
			
			if (file_exists($path . $fileName))
			{
				//echo $_FILES["upload"]["name"] . " already exists. ";
				$result = [
					'error' => [
						"message" => "A file with the same name already exists.",
						"number" => 201
					]
				];
			} else {
				if (!$assetApi->_fileExists($path)) {
					$assetApi->_mkDir($path, 0777, true);
				}
				move_uploaded_file($tmp_name, $path . $fileName);
				$result = [
					"fileName" => $fileName,
					"uploaded" => 1,
					"url" => $assetApi->preparePath($dir . $fileName)
				];
			}
		return $result;
	}
}