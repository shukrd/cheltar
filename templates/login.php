<?php
$assetApi = new \Cheltar\Api\Asset();
//$assetApi->bindSystemDir();
$assetApi->bindSystemStyleFile("/bower_components/bootstrap/dist/css/bootstrap.min.css");
$assetApi->bindSystemStyleFile("/bower_components/font-awesome/css/font-awesome.min.css");
$assetApi->bindSystemStyleFile("/bower_components/Ionicons/css/ionicons.min.css");
$assetApi->bindSystemStyleFile("/dist/css/AdminLTE.css");
//$assetApi->bindSystemStyleFile("/dist/css/skins/skin-blue.min.css");
//$assetApi->bindSystemStyleFile("/bower_components/bootstrap/dist/css/bootstrap.css.map");
$assetApi->bindSystemStyleFile("/plugins/iCheck/square/blue.css");
$assetApi->bindSystemFile('/plugins/iCheck/square/blue.png');
$assetApi->bindSystemScriptFile('/bower_components/jquery/dist/jquery.min.js');
$assetApi->bindSystemScriptFile('/bower_components/bootstrap/dist/js/bootstrap.min.js');
//$assetApi->bindSystemScriptFile('/bower_components/fastclick/lib/fastclick.js');
//$assetApi->bindSystemScriptFile('/dist/js/adminlte.min.js');
$assetApi->bindSystemScriptFile('/plugins/iCheck/icheck.min.js');

//$assetApi->bindSystemFile('/bower_components/font-awesome/fonts/fontawesome-webfont.woff2');


?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Авторизация</title>
  
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- 
  <link rel="stylesheet" href="../../bower_components/bootstrap/dist/css/bootstrap.min.css">
  <link rel="stylesheet" href="../../bower_components/font-awesome/css/font-awesome.min.css">
  <link rel="stylesheet" href="../../bower_components/Ionicons/css/ionicons.min.css">
  <link rel="stylesheet" href="../../dist/css/AdminLTE.min.css">
  <link rel="stylesheet" href="../../plugins/iCheck/square/blue.css">
  -->
<?php echo (new \Cheltar\Api\Asset())->renderStyleLinks() ?>
  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition login-page">
<div class="login-box">
  <!-- /.login-logo -->
  <div class="login-box-body">
    <p class="login-box-msg">Авторизация</p>
	<div>
		<b><?=(empty($content)?"":$content)?></b>
	</div>
    <form id="login" action="/login/" method="POST">
      <div class="form-group has-feedback">
        <input type="login" class="form-control" placeholder="login"  name="login">
        <!--<span class="glyphicon glyphicon-envelope form-control-feedback"></span>-->
      </div>
      <div class="form-group has-feedback">
        <input type="password" class="form-control" placeholder="Password" name="password">
        <!--<span class="glyphicon glyphicon-lock form-control-feedback"></span>-->
      </div>
      <div class="row">
		  <!--
        <div class="col-xs-8">
          <div class="checkbox icheck">
            <label>
              <input type="checkbox"> Remember Me
            </label>
          </div>
        </div>-->
        <!-- /.col -->
        <div class="col-xs-4">
			<button type="submit" class="btn btn-primary btn-block btn-flat">Sign In</button>
        </div>
        <!-- /.col -->
      </div>
    </form>

  </div>
  <!-- /.login-box-body -->
</div>
<!-- /.login-box -->

<!-- jQuery 3 -->

<?php echo (new \Cheltar\Api\Asset())->renderScriptLinks() ?>
<script>
  $(function () {
    $('input').iCheck({
      checkboxClass: 'icheckbox_square-blue',
      radioClass: 'iradio_square-blue',
      increaseArea: '20%' /* optional */
    });
  });
</script>
</body>
</html>
