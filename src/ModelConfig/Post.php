<?php

namespace Cheltar\ModelConfig;

class Post extends BasePageConfig  {

	protected $code = 'post';
	protected $_tableName = 'posts';
	
	function __construct() {
		
		parent::__construct();
		
		$this->fields['list_image'] = new \Cheltar\Field\FileField('list_image', "Изображение для списка");
	}

}
