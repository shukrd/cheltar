<?php
namespace Cheltar\Db;
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of queryBuilder
 *
 * @author user
 */
class queryBuilder {
	const INSERT_ACTION = 1;
	const REPLACE_ACTION = 2;
	const UPDATE_ACTION = 3;
	const DELETE_ACTION = 4;
	
	protected $selectFields = [];
	protected $action;
	protected $actionTable;
	protected $orderBy = [];
	protected $joins = [];
	protected $from;
	protected $andWhere = [];
	protected $groupBy = [];
	protected $andHaving = [];
	protected $fields = [];
	protected $values = [];
	protected $limit = 0;
	protected $offset = 0;
	protected $onDuplicateUpdate = [];			
	
	
	function insertQuery($tableName) {
		$this->action = self::INSERT_ACTION;
		$this->actionTable = $tableName;
		return $this;
	}
	
	function replaceQuery($tableName) {
		$this->action = self::REPLACE_ACTION;
		$this->actionTable = $tableName;
		return $this;
	}
	
	function updateQuery($tableName) {
		$this->action = self::UPDATE_ACTION;
		$this->actionTable = $tableName;
		return $this;
	}
	
	function deleteQuery($tableName) {
		$this->action = self::DELETE_ACTION;
		$this->actionTable = $tableName;
	}

	function addToArray(&$array, $value, $uniqName = null) {
		if (isset($uniqName)) {
			$array[$uniqName] = $value;
		} else {
			$array[] = $value;
		}
	}
			
	function addSelectField($value, $uniqName = null) {

		$this->addToArray($this->selectFields, $value, $uniqName);
				
		return $this;
	}
	
	function clearSelectField() {
		$this->selectFields = [];
		return $this;
	}
			
	function addOrderBy($order, $uniqName = null) {
		
		$this->addToArray($this->orderBy, $order, $uniqName);

		return $this;
	}
	
	function addJoin($join, $uniqName = null) {
		
		$this->addToArray($this->joins, $join, $uniqName);

		return $this;
	}
	
	function setFrom($from) {
		$this->from = $from;
		return $this;
	}

	function addAndWhere($condition, $uniqName = null) {
		$this->addToArray($this->andWhere, $condition, $uniqName);

		return $this;
	}
	
	function addGroupBy($group, $uniqName = null) {
		$this->addToArray($this->groupBy, $group, $uniqName);

		return $this;
	}
			
	function addAndHaving($having, $uniqName = null) {
		
		$this->addToArray($this->andHaving, $having, $uniqName);

		return $this;
	}
	
	function setFields(array $fields) {
		$this->fields = $fields;
		return $this;
	}
	
	function setValues($values) {
		$this->values = [$values];
		return $this;
	}
	
	function addValues($values) {
		$this->values[] = [$values];
		return $this;		
	}
	
	function setLimit($limit) {
		$this->limit = (int)$limit;
		return $this;
	}
	
	function getLimit() {
		return $this->limit;
	}
	
	function setOffset($offset) {
		$this->offset = $offset;
		return $this;
	}
	
	function getOffset() {
		return $this->offset;
	}
	
	function getQuery() {
		$queryArr = [];
		if ($this->action == self::INSERT_ACTION) {
			$queryArr[] = "INSERT INTO `" . $this->actionTable . "`";
		} elseif ($this->action == self::REPLACE_ACTION) {
			$queryArr[] = "REPLACE INTO `" . $this->actionTable . "`";
		} elseif ($this->action == self::UPDATE_ACTION) {
			$queryArr[] = "UPDATE `" . $this->actionTable . "`";
		} elseif ($this->action == self::DELETE_ACTION) {
			$queryArr[] = "DELETE FROM `" . $this->actionTable . "`";
		} else {
			if (empty($this->selectFields)) {
				$this->addSelectField('*');
			}
		}
		
		if (!empty($this->fields)) {
			$queryArr[] = "(`" . implode("`, `", $this->fields) . "`)";
		}
		
		if (!empty($this->values)) {
			$rows = [];
			foreach($this->values as $row) {
				$rowSlash = [];
				foreach ($row as $value) {
					$rowSlash[] = addslashes($value);
				}
				$rows[] = "('" . implode("', '", $rowSlash) . "')";
			}
			$queryArr[] = "VALUES " . implode(",\n", $rows);
		}
		
		if (!empty($this->selectFields)) {
			$queryArr[] = "SELECT " . implode(', ', $this->selectFields);
			$queryArr[] = "FROM " . $this->from;
		}
		
		if (!empty($this->joins)) {
			$queryArr[] = implode("\n", $this->joins);
		}
		
		if (!empty($this->andWhere)) {
			$queryArr[] = "WHERE";
			$queryArr[] = implode("\n AND ", $this->andWhere);
		}
		
		if (empty($this->groupBy) && !empty($this->orderBy)) {
			$queryArr[] = "ORDER BY " . implode(", ", $this->orderBy);
		}
		
		if (!empty($this->groupBy)) {
			$queryArr[] = "GROUP BY " . implode(", ", $this->groupBy);
		}

		if (!empty($this->andHaving)) {
			$queryArr[] = "HAVING";
			$queryArr[] = implode("\n AND ", $this->andHaving);
		}
		
		if (!empty($this->groupBy) && !empty($this->orderBy)) {
			$queryArr[] = "ORDER BY " . implode(", ", $this->orderBy);
		}

		if (!empty($this->limit)) {
			$queryArr[] = "LIMIT " . $this->limit;
		}
		
		if (!empty($this->offset)) {
			$queryArr[] = "OFFSET " . $this->offset;
		}
		
		if (!empty($this->onDuplicateUpdate)) {
			$arr = [];
			foreach ($this->onDuplicateUpdate as $key => $value) {
				$arr[] = "`" . $key . "` = '" . addslashes($value) . "'";
			}
			$queryArr[] = "ON DUPLICATE KEY UPDATE " . implode(",", $arr);
		}

		
		
		$queryStr = implode("\n", $queryArr);
		return $queryStr;
	}
	
	function setOnDuplicateUpdate($array) {
		$this->onDuplicateUpdate = $array;
		return $this;
	}
}