<?php

namespace Cheltar\Field;

class EnumField extends BaseField {
	
	protected $defaultFormControl = "\Cheltar\Control\SelectControl";
	protected $defaultFilterControl = "\Cheltar\Control\SelectControl";
	
	protected $options = [];
			
	function getOptions() {
		return $this->options;
	}
	
	function getFilterOptions() {
		$res = $this->getOptions();
		$res = ['' => ''] + $res;
		return $res;
	}
	
	function setOptions($options) {
		$this->options = $options;
		return $this;
	}
	
	function prepareQueryBuilder($qb) {
		
		$options = $this->getOptions();
$str = "CASE main." . $this->getCode() . " \n";

		foreach ($options as $key => $value) {
			$str .= "WHEN " . $key . " THEN '" . addslashes($value) . "'\n";
		}
		
		$str .= "END AS " . $this->getCode();
		$qb->addSelectField($str);
	}
}
