<?php
namespace Cheltar\Db;

class db {
	
	protected $pdo;
	
	function __construct($dsn, $user, $password) {
		
		$this->pdo = new \PDO($dsn, $user, $password);
		
	}
	
	function getPDO() {
		return $this->pdo;
	}
	
}