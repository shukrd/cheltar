<?php

namespace Cheltar\Field;

class FileField extends BaseField {
	
	protected $defaultFormControl = "\Cheltar\Control\FileControl";
	
	function prepareBeforeSave($data, $modelConfig) {
		
		$result = [];
		$file = parent::prepareBeforeSave($data, $modelConfig);
		//print_r($data); echo "<br>";
		//print_r($_FILES);
		

		if (!empty($_FILES[$modelConfig->getCode()]['name'][$this->getCode()])) {
			$arr = $_FILES[$modelConfig->getCode()];
			$serviceUploadFile = new \Cheltar\Service\uploadFile();
			$result = $serviceUploadFile->upload($arr, $this->getCode());			
		}
		
		if (!empty($_POST[$modelConfig->getCode()][$this->getCode()]['delete'])) {
			//unset($_SERVER['DOCUMENT_ROOT'] . "/_upload" . $data[$this->getCode()]);
			//todo физическое удаление файлов
			//print_r($_POST[$modelConfig->getCode()][$this->getCode()]['delete']);
			$file = '';
		} elseif (!empty($result['url'])) {
			$file = $result['url'];
		}
		//print_r($file);
		return $file;
	}
}
