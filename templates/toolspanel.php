<ul class="toolspanel">
<?php
	foreach ($tools as $options) {
		
		$html = '';
		$text = empty($options['text'])?"":$options['text'];
		$image = empty($options['image'])?"":$options['image'];
		if ($image) {
			$html = "<img src='" . $image . "' alt='" . $text . "'>";
		} else {
			$html = $text;
		}
		
?>
	<li>
		<a href="<?= empty($options['link'])?"":$options['link']?>"><?=$html?></a>
	</li>
<?php
	}
?>

</ul>