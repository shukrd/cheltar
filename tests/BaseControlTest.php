<?php
use PHPUnit\Framework\TestCase;

class BaseControlTest extends TestCase
{
    public function testControl()
    {
        $object = new \Cheltar\Control\BaseControl('test1', 'test2');
		$this->assertEquals($object->getName(), 'test1');
		$this->assertEquals($object->getValue(), 'test2');
		
		$object->setName('test3')
			->setType('test4')
			->setClassName('test5')
			->setValue('test6');
		
		$this->assertEquals($object->getName(), 'test3');
		$this->assertEquals($object->getType(), 'test4');
		$this->assertEquals($object->getClassName(), 'test5');
		$this->assertEquals($object->getValue(), 'test6');
		
		$this->assertEquals($object->render(), '<input type="test4" name="test3" value="test6" class="test5">');
		
		$object->setClassName('');
		$this->assertEquals($object->render(), '<input type="test4" name="test3" value="test6">');
		
		$object->setName(null);
		$this->assertEquals($object->render(), '<input type="test4" value="test6">');
		
		$object->setValue(0);
		$this->assertEquals($object->render(), '<input type="test4" value="0">');
    }
	

}
?>