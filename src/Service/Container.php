<?

namespace Cheltar\Service;

class Container {

	protected $constructorList;
	protected $elementList;
	
	
	public function setConstructor($name, $className = null, $argumentList = []) {
		$this->constructorList[$name] = ['className' => $className, 'argumentList' => $argumentList];
	}
	
	public function setElement($name, $value) {
		$this->elementList[$name] = $value;
	}
	
	public function __get($name) {
		if (isset($this->elementList[$name])) {
			return $this->elementList[$name];
		}
		
		if (isset($this->constructorList[$name])) {
			try {
				$shapeClass = new \ReflectionClass($this->constructorList[$name]['className']);
				return $this->elementList[$name] = call_user_func_array([&$shapeClass, 'newInstance'], $this->constructorList[$name]['argumentList']);
			} catch (Exception $e) {
				echo $e->getMessage()."\n";
			}
		}
	}
	
}

?>