<?php
namespace Cheltar\Control;

class Calendar {
	protected $dates = [];
	protected $templateText = '';
	protected $baseUrl;		
	function setDates(array $dates) {
		$this->dates = $dates;
		return $this;
	}
	
	function getDates() {
		return $this->dates;
	}
	
	function addDate($day) {
		$this->dates[$day] = 1;
		return $this;
	}
	
	function getTemplateText() {
		if (empty($this->templateText)) {
			$this->templateText = \Cheltar\Helper\Template::loadTemplateText("calendar.php");
		}
		
		return $this->templateText;
	}
	
	function setTemplateText($templateText) {
		$this->templateText = $templateText;
		return $this;
	}
	
	function setBaseUrl($url) {
		$this->baseUrl = $url;
		return $this;
	}
	
	function getBaseUrl() {
		if (empty($this->baseUrl)) {
			return "/";
		} else {
			return $this->baseUrl;
		}
	}
	
	function render() {
		
		
		
		$month_names = ["январь","февраль","март","апрель","май","июнь","июль","август","сентябрь","октябрь","ноябрь","декабрь"];
		if (isset($_GET['y'])) $y=$_GET['y'];
		if (isset($_GET['m'])) $m=$_GET['m'];
		if (isset($_GET['date']) AND strstr($_GET['date'],"-")) list($y,$m)=explode("-",$_GET['date']);
		if (!isset($y) OR $y < 1970 OR $y > 2037) $y=date("Y");
		if (!isset($m) OR $m < 1 OR $m > 12) $m=date("m");

		$month_stamp = mktime(0,0,0,$m,1,$y);
		$day_count = date("t",$month_stamp);
		$weekday=date("w",$month_stamp);
		if ($weekday == 0) $weekday = 7;
		$start=-($weekday-2);
		$last=($day_count+$weekday-1) % 7;
		if ($last==0) $end=$day_count; else $end=$day_count+7-$last;
		$today=date("Y-m-d");
		//$prev = date('?\m=m&\y=Y',mktime (0,0,0,$m-1,1,$y));  
		//$next=date('?\m=m&\y=Y',mktime (0,0,0,$m+1,1,$y));
		
		$baseLink = new \Cheltar\Model\Link($this->getBaseUrl());
		$baseLink->addParam('y', $y)
			->addParam('m', $m);
		
		$prevDateMonth = date('m',mktime (0,0,0,$m-1,1,$y));
		$prevDateYear = date('Y',mktime (0,0,0,$m-1,1,$y));
		$nextDateMonth = date('m',mktime (0,0,0,$m+1,1,$y));
		$nextDateYear = date('Y',mktime (0,0,0,$m+1,1,$y));
		
		$prevLink = (clone $baseLink);
		$prevLink->addParam('y', $prevDateYear)
			->addParam('m', $prevDateMonth);		
		$nextLink = (clone $baseLink);
		$nextLink->addParam('y', $nextDateYear)
			->addParam('m', $nextDateMonth);
		
		foreach ($this->dates as $key => $date) {
			$link = clone $baseLink;
			$link->addParam('d', $key);
			$this->dates[$key] = $link->getUrl();
		}
		
		$data = [
			"day_count" => $day_count,
			"y" => $y,
			"m" => $m,
			"start" => $start,
			"end" => $end,
			"prev" => $prevLink->getUrl(),
			"next" => $nextLink->getUrl(),
			"today" => $today,
			"month_names" => $month_names,
			"fill" => $this->dates
		];
		
		//$paginator = $this->getPaginatorArray();
		//$templateText = \Cheltar\Helper\Template::loadTemplateText("breadcrumbs.php");
		return \Cheltar\Helper\Template::render($data, $this->getTemplateText());
	}
}
