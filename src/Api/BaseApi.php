<?php

namespace Cheltar\Api;

class BaseApi 
{
	use \Cheltar\Pattern\Singleton;
	function getUrlSegments($url) {
		
		if (empty($url)) {
			return [];
		}
		
		$parts = explode('?', $url);
		
		if (empty($parts[0])) {
			return [];
		}
		
		$segments = explode('/', $parts[0]);
		
		return array_values(array_filter($segments));
	}
}
