<?php

namespace Cheltar\Field;

class BaseField {
	protected $code;
	protected $caption;
	protected $tableName;
	protected $tableFieldCode;

	protected $isKey;
	protected $dataType;
	protected $defaultFormControl = "\Cheltar\Control\StringControl";
	protected $defaultFilterControl = "\Cheltar\Control\StringControl";
	protected $inDefaultForm = true;
	protected $inDefaultTable = true;
	protected $defaultValue = null;
	
	protected $onlyRead = false;


	protected $modelObject; // на случай, если для корректного формирования данных (например списка) требуется учитывать данные текущей модели

	function __construct($code, $caption = '') {
		
		$this->code = $code;
//		$this->tableName = $tableName;
		
		if (empty($tableFieldCode)) {
			$tableFieldCode = $code;
		}
		
		$this->tableFieldCode = $tableFieldCode;
		
		$this->caption = $caption;
	}
	
	function getCode() {
		return $this->code;
	}


	function getTableName() {
		return $this->tableName;
	}
	
	function setTableFieldCode($tableFieldCode) {
		$this->tableFieldCode = $tableFieldCode;
		return $this;
	}
	
	function getTableFieldCode() {
		
		if ($this->tableFieldCode) {
			return $this->tableFieldCode;
		} else {
			return $this->code;
		}
	}

	function setKey($isKey) {
		$this->isKey = (boolean) $isKey;
		return $this;
	}
	
	function isKeyField() {
		return $this->isKey;
	}
	
	function getDataType() {
		return $this->dataType;
	}
	
	function setDefaultFormControl($controlClass) {
		if (!empty($controlClass)) {
			$this->defaultFormControl = $controlClass;
		}
		return $this;
	}
	
	function getDefaultFormControl() {
		return $this->defaultFormControl;
	}
	
	function getDefaultFilterControl() {
		return $this->defaultFilterControl;
	}
	
	function setInDefaultForm($inDefaultForm) {
		$this->inDefaultForm = (boolean) $inDefaultForm;
		return $this;
	}
	
	function getInDefaultForm() {
		return $this->inDefaultForm;
	}
	
	function setCaption($caption) {
		$this->caption = $caption;
		
		return $this;
	}
	
	function getCaption() {
		return $this->caption;
	}
	
	function setDefaultValue($defaultValue) {
		$this->defaultValue  = $defaultValue;
		return $this;
	}
			
	function getDefaultValue() {
		return $this->defaultValue;
	}
	
	function setModelObject($modelObject) {
		$this->modelObject = $modelObject;
		return $this;
	}
	
	function getModelObject() {
		return $this->modelObject;
	}
	
	function setInDefaultTable($inDefaultTable) {
		$this->inDefaultTable = $inDefaultTable;
		return $this;
	}
	
	function getInDefaultTable() {
		return $this->inDefaultTable;
	}
	
	function prepareQueryBuilder($qb) {
		$qb->addSelectField('main.' . $this->code);
	}
	
	/**
	 * 
	 * @param type $value
	 * @param type $modelConfig
	 * @return type
	 */
	function prepareBeforeSave($data, $modelConfig) {
		if (!isset($data[$this->code])) {
			return false;
		}
		return $data[$this->code];
	}
	
	function getter($data, $modelConfig) {
		return $data[$this->code];
	}
	
	function isOnlyRead() {
		return $this->onlyRead;
	}
	
	function setOnlyRead($onlyRead) {
		$this->onlyRead = (boolean) $onlyRead;
		return $this;
	}
}
