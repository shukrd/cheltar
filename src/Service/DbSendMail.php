<?
namespace Cheltar\Service;
use \Cheltar\App\app;

class DbSendMail implements SendMailInterface {
	
	protected $tableName = '__test_mail_log';
	
	function __construct($config = []) {
		$this->createTable();
	}
	
	protected function createTable() { 
		app::getContainer()->db->getPdo()->query("
CREATE TABLE IF NOT EXISTS `{$this->tableName}` (
	`id` INT(11) NOT NULL AUTO_INCREMENT,
	`log` TEXT NOT NULL,
	PRIMARY KEY (`id`)
)ENGINE=InnoDB;
;
		");
	}

	protected function insertDbLog($to, $from, $subject, $message) {
		$text = addslashes("to:{$to};from:{$from};subject:{$subject};message:{$message}");
		app::getContainer()->db->getPdo()->query("INSERT INTO {$this->tableName} SET `log` = '{$text}'");
	}
	
	function send($to, $from, $subject, $message) {
		$this->insertDbLog($to, $from, $subject, $message);
	}
}

?>