<div class="row">
        <div class="col-xs-12">
          <div class="box">
            <div class="box-header">
              <h3 class="box-title">Hover Data Table</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
				
<?php
	foreach ((array)$toolsLinks as $tool) {
?>
				<a href="<?=$tool['link']?>"><?=$tool['text']?></a>
<?php
	}
?>				
				
              <div id="example2_wrapper" class="dataTables_wrapper form-inline dt-bootstrap"><div class="row"><div class="col-sm-6"></div><div class="col-sm-6"></div></div><div class="row"><div class="col-sm-12">
				<form>
					<table class="table table-bordered table-hover dataTable" role="grid" >
                <tr role="row">
					<th></th>
<?php
	foreach ($filter['captions'] as $cell) {
?>
				<th rowspan="1" colspan="1"><?=$cell?></th>
<?php
	}
?>		

				</tr>
                <tr role="row">
					<td></td>
<?php
	foreach ($filter['controls'] as $cell) {
?>
				<td rowspan="1" colspan="1"><?=$cell?></td>
<?php
	}
?>						

				</tr>
					</table></form>                

						  <table id="example2" class="table table-bordered table-hover dataTable" role="grid" aria-describedby="example2_info">
						  
				<thead>
                <tr role="row">
<?php
	foreach ($header as $cell) {
?>
				<th tabindex="0" aria-controls="example2" rowspan="1" colspan="1"><?=$cell?></th>
<?php
	}
?>						

				</tr>
                </thead>
                <tbody>

<?php
foreach ($table as $row) {
?>
					<tr>
<?php
	foreach ($row as $cell) {
?>
				<td><?=$cell?></td>
<?php
	}
?>
					</tr>
<?php
}
?>
				</tbody>
                <tfoot>
                <tr role="row">
<?php
	foreach ($header as $cell) {
?>
				<th tabindex="0" aria-controls="example2" rowspan="1" colspan="1"><?=$cell?></th>
<?php
	}
?>						

				</tr>
                </tfoot>
              </table></div></div>
				  <div class="row">
					  <div class="col-sm-5">
						  <!--<div class="dataTables_info" id="example2_info" role="status" aria-live="polite">Showing 1 to 10 of 57 entries</div>-->
							  
					  </div>
					  <div class="col-sm-7">
<?=$paginator?>
					  </div></div></div>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->

        </div>
        <!-- /.col -->
      </div>