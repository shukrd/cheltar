<?php

namespace Cheltar\Field;

class SefField extends BaseField {
	
	protected $defaultFormControl = "\Cheltar\Control\StringControl";
	
	function prepareBeforeSave($data, $modelConfig) {
		$sef = $data[$this->code];
		if (empty($sef)) {
			$sef = $this->translit($data['title']);
		}
		if (!$this->isFieldUniq($sef, $data, $modelConfig)) {
			$i = 1;	
			do {
				$sef = $this->translit($data['title']) . "-" . $i;
				$i++;
			} while (!$this->isFieldUniq($sef, $data, $modelConfig));
		}
		return $sef;
	}
	
	function isFieldUniq($sef, $data, $modelConfig) {
		global $db;
		$qb = new \Cheltar\Db\queryBuilder();

		$qb->addSelectField('COUNT(*) as cnt')
				->setFrom($modelConfig->getTableName())
				->addAndWhere($this->getTableFieldCode() . " = '" . addslashes($sef) . "'")
				->addGroupBy($this->getTableFieldCode());
		
		if (!empty($data['id'])) {
			$qb->addAndWhere("id != '" . addslashes($data['id']) . "'");
		}

		$res = $db->getPdo()->query($qb->getQuery(), \PDO::FETCH_ASSOC);

		$result = $res->fetch();
		return empty($result['cnt']);
	}
	
	function translit($s) {
	  $s = (string) $s; // преобразуем в строковое значение
	  $s = strip_tags($s); // убираем HTML-теги
	  $s = str_replace(array("\n", "\r"), " ", $s); // убираем перевод каретки
	  $s = preg_replace("/\s+/", ' ', $s); // удаляем повторяющие пробелы
	  $s = trim($s); // убираем пробелы в начале и конце строки
	  $s = function_exists('mb_strtolower') ? mb_strtolower($s) : strtolower($s); // переводим строку в нижний регистр (иногда надо задать локаль)
	  $s = strtr($s, array('а'=>'a','б'=>'b','в'=>'v','г'=>'g','д'=>'d','е'=>'e','ё'=>'e','ж'=>'j','з'=>'z','и'=>'i','й'=>'y','к'=>'k','л'=>'l','м'=>'m','н'=>'n','о'=>'o','п'=>'p','р'=>'r','с'=>'s','т'=>'t','у'=>'u','ф'=>'f','х'=>'h','ц'=>'c','ч'=>'ch','ш'=>'sh','щ'=>'shch','ы'=>'y','э'=>'e','ю'=>'yu','я'=>'ya','ъ'=>'','ь'=>''));
	  $s = preg_replace("/[^0-9a-z-_ ]/i", "", $s); // очищаем строку от недопустимых символов
	  $s = str_replace(" ", "-", $s); // заменяем пробелы знаком минус
	  return $s; // возвращаем результат
	}
	
}
