<?php

namespace Cheltar\Control;

class FormatTextControl extends BaseControl {
	
	function render() {

		$assetApi = new \Cheltar\Api\Asset();

		$assetApi->bindSystemScriptFile('/bower_components/ckeditor/ckeditor.js');
		$assetApi->bindSystemScriptFile('/bower_components/ckeditor/config.js');
		$assetApi->bindSystemScriptFile('/bower_components/ckeditor/lang/ru.js');
		$assetApi->bindSystemScriptFile('/bower_components/ckeditor/styles.js');

		$assetApi->bindSystemScriptFile('/bower_components/ckeditor/plugins/image/dialogs/image.js');
		$assetApi->bindSystemScriptFile('/bower_components/ckeditor/plugins/link/dialogs/link.js');
		$assetApi->bindSystemScriptFile('/bower_components/ckeditor/plugins/link/dialogs/anchor.js');
		$assetApi->bindSystemScriptFile('/bower_components/ckeditor/plugins/table/dialogs/table.js');
		$assetApi->bindSystemScriptFile('/bower_components/ckeditor/plugins/specialchar/dialogs/lang/ru.js');
		$assetApi->bindSystemScriptFile('/bower_components/ckeditor/plugins/specialchar/dialogs/specialchar.js');
		
		$assetApi->bindSystemScriptFile('/bower_components/ckeditor/plugins/about/dialogs/about.js');
		$assetApi->bindSystemScriptFile('/bower_components/ckeditor/plugins/pastefromword/filter/default.js');
		//$assetApi->bindSystemScriptFile('/bower_components/ckeditor/plugins/uploadfile/plugin.js');
		//$assetApi->bindSystemScriptFile('/bower_components/ckeditor/plugins/uploadimage/plugin.js');
		//$assetApi->bindSystemScriptFile('/bower_components/ckeditor/plugins/imageuploader/plugin.js');
			//shukrd\cheltar\assets\bower_components\ckeditor\plugins\pastefromword\filter
		
		
		$assetApi->bindSystemStyleFile('/bower_components/ckeditor/skins/moono-lisa/editor.css');
		$assetApi->bindSystemStyleFile('/bower_components/ckeditor/plugins/scayt/skins/moono-lisa/scayt.css');
		$assetApi->bindSystemStyleFile('/bower_components/ckeditor/plugins/scayt/dialogs/dialog.css');
		$assetApi->bindSystemStyleFile('/bower_components/ckeditor/plugins/tableselection/styles/tableselection.css');
		$assetApi->bindSystemStyleFile('/bower_components/ckeditor/plugins/wsc/skins/moono-lisa/wsc.css');
		$assetApi->bindSystemStyleFile('/bower_components/bootstrap/dist/css/bootstrap.min.css.map');
		$assetApi->bindSystemStyleFile('/bower_components/ckeditor/contents.css');
		$assetApi->bindSystemStyleFile('/bower_components/ckeditor/skins/moono-lisa/dialog.css');
		
		$assetApi->bindSystemFile('/bower_components/ckeditor/skins/moono-lisa/icons.png');
		$assetApi->bindSystemFile('/bower_components/ckeditor/skins/moono-lisa/icons_hidpi.png');		

		$arr = [];
		
		if (!empty($this->name)) {
			$arr[] = "name=\"" . $this->name . "\"";
		}

		
		
		if (!empty($this->className)) {
			$arr[] = "class=\"" . $this->className . " format_text\"";
		}

		return "<textarea id='ed1'" . implode($arr, " ") . ">" . htmlentities($this->value) . "</textarea>";
	}
}