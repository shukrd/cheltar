<?php

namespace Cheltar\ModelConfig;

class Menu extends BaseConfig  {

	protected $code = 'menu';
	protected $_tableName = 'menus';
	
	function __construct() {
		
		$this->fields['id'] = (new \Cheltar\Field\IntegerField('id', 'Номер'))
				->setKey(true)
				->setDefaultFormControl("\Cheltar\Control\HiddenControl");
				
		$this->fields['title'] = new \Cheltar\Field\StringField('title', "Название");
		
		$this->fields['code'] = new \Cheltar\Field\StringField('code', "Символьный код");
		
		$this->fields['url'] = new \Cheltar\Field\StringField('url', "Адрес");
	
		$this->fields['active'] = new \Cheltar\Field\EnumField('active', 'Включен');
		$this->fields['active']->setOptions([0 => 'нет', 1 => 'да'])
				->setDefaultValue(1);
		
		$this->fields['image'] = new \Cheltar\Field\FileField('image', "Изображение");
		
		$this->fields['parent_id'] = (new \Cheltar\Field\RelationToOneField('parent_id', 'Родительский узел'))
				->setRelatedType('\Cheltar\ModelConfig\Menu');
		
		$this->fields['order'] = new \Cheltar\Field\StringField('order', "Сортировка");
		
		$this->fields['target'] = new \Cheltar\Field\EnumField('target', 'Где открыть');
		$this->fields['target']->setOptions([0 => 'в этом же окне', 1 => 'в новом окне']);
		
		$this->modelClassName = "\Cheltar\Model\baseModel";
	}
}
