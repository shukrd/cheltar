<?php

use PHPUnit\Framework\TestCase;
use \Cheltar\App\app;

class testsConfig {

	static $ind = 0;

	function __construct() {
		self::$ind = self::$ind + 1;
	}

}

class appTest extends TestCase {

	public function testConstruct() {
		$obj = new \Cheltar\App\app();
		$this->assertTrue(true);
	}

	public function testConfigObject() {
		$this->assertFalse(app::getConfigObject('testsConfig1'));
		$obj = app::getConfigObject('testsConfig');
		$this->assertEquals(get_class($obj), 'testsConfig');
		$this->assertEquals($obj::$ind, 1);

		$obj = app::getConfigObject('testsConfig');
		$this->assertEquals(get_class($obj), 'testsConfig');
		$this->assertEquals($obj::$ind, 1);
	}

}

?>