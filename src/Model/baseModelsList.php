<?php
namespace Cheltar\Model;

class baseModelsList {
	
	protected $modelConfig;
	
	protected $offset, $limit;
	
	protected $queryBuilder;
			
	protected $filters;
	
	protected $order;

	protected $conditions = [];

	protected $modelFactory;

	protected $db;
	
	function __construct($modelConfig) {
		global $db;
		$this->modelConfig = $modelConfig;
		$this->db = $db;
	}
	
	function setOffset($offset) {
		$this->offset = $offset;
		return $this;
	}
	
	function getOffset() {
		return $this->offset;
	}
	
	function setLimit($limit) {
		$this->limit = $limit;
		return $this;
	}
	
	function getLimit() {
		return $this->limit;
	}
	
	function getQueryBuilder() {
		if (empty($this->queryBuilder)) {
			$this->queryBuilder = $this->getModelFactory()->prepareQueryBuilder($this->filters);
		}
		return $this->queryBuilder;
	}
	
	// @depricated
	function setFilters($filters) {
		$this->conditions = [];
		foreach ($filters as $key => $filter) {
			$this->addCondition('=', $key, $filter);
		}
		return $this;
	}
	
	function getModelsArray() {
		
		global $db;
		
		$this->queryBuilder = $qb = $this->getModelFactory()->prepareQueryBuilder([]);
		
		foreach ($this->conditions as $row) {
			switch ($row[0]) {
				case '=' :
					$qb->addAndWhere($row[1] . " = '" . addslashes($row[2]) . "'");
					break;
				case 'LIKE' :
					$qb->addAndWhere($row[1] . " LIKE '" . addslashes($row[2]) . "'");
					break;
			}
		}
		
		if (!empty($this->order)) {
			$qb->addOrderBy($this->order);
		}
		
		if (!empty($this->limit)) {
			$qb->setLimit($this->limit);
		}
		if (!empty($this->offset)) {
			$qb->setOffset($this->offset);
		}
		
		$result = [];
		//print_r($qb->getQuery());
		$res = $db->getPDO()->query($qb->getQuery(), \PDO::FETCH_ASSOC);
		if (empty($res)) {
			return [];
		}
		
		foreach ($res as $row) {
			$object = $this->getModelFactory()->getModelObject();
			$object->setData($row);
			$result[] = $object;
		}
		return $result;
		
		//return $this->getModelFactory()->getModelsArrayByFields($this->filters, $this->order);
	}
	
	function getConfigObject() {
		return $this->modelConfig;
	}
	
	function getModelFactory() {
		
		if (empty($this->modelFactory)) {
			$this->modelFactory = new \Cheltar\Model\baseModelFactory($this->getConfigObject());
		}
		
		return $this->modelFactory;
	}
	
	function setOrder($order) {
		$this->order = $order;
		return $this;
	}
	
	function getTotalCount () {
		$totalCountQb = clone $this->getQueryBuilder();
		
		$totalCountQb->clearSelectField()
				->addSelectField("COUNT(*) as cnt")
				->setLimit(null)
				->setOffset(0);
				//print_r($totalCountQb->getQuery());
		return $this->db->getPdo()->query($totalCountQb->getQuery(), \PDO::FETCH_ASSOC)->fetchColumn();
	}
	
	function addCondition($operand, $field, $x) {
		$this->conditions[] = func_get_args();
		return $this;
	}
}