<?php

namespace Cheltar\Helper;


class Calendar {
	static function renderCalendar($configObject, $filters = []) {
		
		global $db;
		
		if (!$configObject->getFieldParams('create_datetime')) {
			return '';
		}
		
		$calendarControl = new \Cheltar\Control\Calendar();
		$calendarControl->setBaseUrl("/news/");
		$postModelList = new \Cheltar\Model\baseModelsList($configObject);
		
		$qb = $postModelList->getModelFactory()->prepareQueryBuilder($filters);
		
		

		if (isset($_GET['y'])) {
			$y = $_GET['y'];
		} else {
			$y = date('Y');
		}
		
		if (isset($_GET['m'])) {
			$m = $_GET['m'];
		} else {
			$m = date('m');
		}
			
		
		$date = addslashes("$y-$m%");
		$qb->addAndWhere("`create_datetime` LIKE '$date'")
			->addGroupBy("`create_datetime`");
		

		
		//print_r($qb->getQuery());
		
		$res = $db->getPDO()->query($qb->getQuery(), \PDO::FETCH_ASSOC);
		$days = [];
		if (!empty($res)) {
			foreach ($res as $row) {
				$day = substr($row['create_datetime'], 8, 2);
				$calendarControl->addDate((int)$day);
			}
		}
		return $calendarControl->render();
	}
}