<?php

namespace Cheltar\Control;

class Paginator {
	
	protected $countOnPage;
	protected $queryBuilder;
	protected $paramName = 'p';
	protected $db;
	protected $urlObject;
	protected $totalCount;
			
	
	function __construct() {
		global $db;
		$this->db = $db;
	}
			
	function setCountOnPage($count) {
		$this->countOnPage = $count;
		return $this;
	}
	
	function setQueryBuilder($qb) {
		$this->queryBuilder = $this->prepareTotalCountQueryBuilder($qb);
		return $this;
	}
	
	function setParamName($p) {
		$this->paramName = $p;
		return $this;
	}
	
	function setUrl($url) {
		$this->urlObject = new \Cheltar\Model\Link($url);
		return $this;
	}
	
	protected function prepareTotalCountQueryBuilder($qb) {
		$totalCountQb = clone $qb;
		$totalCountQb->clearSelectField()
				->addSelectField("COUNT(*) as cnt");

		return $totalCountQb;
	}
	
	function getTotalCount () {

		//$countQb = $this->prepareTotalCountQueryBuilder();
		if (!isset($this->totalCount)) {
			$res = $this->db->getPdo()->query($this->queryBuilder->getQuery(), \PDO::FETCH_ASSOC);
			if ($res) {
				$this->totalCount = $res->fetchColumn();
			} else {
				$this->totalCount = 0;
			}
		}
		return $this->totalCount;
	}
	
	function getTotalPageCount() {
		
		$totalCount = $this->getTotalCount();
		$pageCount = ceil($totalCount/$this->countOnPage);
		return $pageCount;
	}
	
	function getPaginatorArray() {
		
		$pageCount = $this->getTotalPageCount();

		$result = [
			'list' => []
		];
		$params = $this->urlObject->getParams();
		$currentPage = 0;
		if (isset($params[$this->paramName])) {
			$currentPage = $params[$this->paramName];
		}
		for ($i=0; $i<$pageCount; $i++) {
			$page = [
				"text" => $i+1,
				"url" => $this->urlObject->addParam($this->paramName, $i)->getUrl()
			];
			
			if ($i == $currentPage) {
				$page['active'] = true;
			}
			
			$result['list'][] = $page;
		}
		return $result;
	}

	function setTotalCount($count) {
		$this->totalCount = $count;
		return $this;
	}
	
	function render() {
		$paginator = $this->getPaginatorArray();
		$templateText = \Cheltar\Helper\Template::loadTemplateText("paginator.php");
		return \Cheltar\Helper\Template::render(['paginator' => $paginator], $templateText);
	}
}