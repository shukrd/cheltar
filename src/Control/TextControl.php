<?php

namespace Cheltar\Control;

class TextControl extends BaseControl {
	
	function render() {
		
		$arr = [];
		
		if (!empty($this->name)) {
			$arr[] = "name=\"" . $this->name . "\"";
		}

		if (!empty($this->className)) {
			$arr[] = "class=\"" . $this->className . "\"";
		}
		
		return "<textarea " . implode($arr, " ") . ">" . htmlentities($this->value) . "</textarea>";
	}			
}