<?php

namespace Cheltar\Helper;


class Factory {
	use \Cheltar\Pattern\Singleton;
	protected $data = [];
	
	function getData($keys) {
		$key = $this->getKey($keys);
		if (isset($data[$key])) {
			return $data[$key];
		} else {
			return false;
		}
	}
	
	function setData($keys, $value) {
		$key = $this->getKey($keys);
		$this->data[$key] = $value;
	}
			
	function getModelApi($modelConfig) {
		$keys = ['modelapi', $modelConfig->getApiClassName()];
		if ($this->getData($keys)) {
			$modelApi = $this->getData($keys);
		} else {
			$className = $modelConfig->getApiClassName();
			$modelApi = new $className($modelConfig);
			$this->setData($keys, $modelApi);
		}
		
		return $modelApi;
	}
	
	function getKey($keys) {
		return implode(",", (array)$keys);
	}
	
	function getModelController($modelConfig) {
		$className = $modelConfig->getControllerClassName();
		$modelApi = new $className($modelConfig);
		return $modelApi;
	}
}