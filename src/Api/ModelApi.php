<?php

namespace Cheltar\Api;

class ModelApi
{
	
	protected $modelConfig;
			
	function __construct($modelConfig) {
		$this->modelConfig = $modelConfig;
	}
	
	function getModelConfig() {
		return $this->modelConfig;
	}
	
	function getUrl($modelObject, $action, $mode = '') {
		switch ($mode) {
			case 'admin':
				return $this->getAdminUrl($modelObject, $action);
				break;
			default :
				$parent = $modelObject->getParent();
				$url = "/";
				if ($parent) {
					$url = $parent->getUrl($mode);
				}
				$url .= $modelObject->sef . "/";
				return $url;
		}
	}
	
	function getAdminUrl($modelObject, $action) {
		if ($action == 'index') {
			$url = "/admin/" . $this->getModelConfig()->getCode() . "/";
		} elseif ($action == 'create') {
			$url = "/admin/" . $this->getModelConfig()->getCode() . "/" . $action ."/";
		} else {
			$url = "/admin/" . $this->getModelConfig()->getCode() . "/" . $action . "/" . $modelObject->getFirstKey() . "/";
		}
		return $url;
	}
}
