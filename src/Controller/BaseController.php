<?php
namespace Cheltar\Controller;

use \Cheltar\App\app;
use \Cheltar\Helper\Factory;
class BaseController {
	
	protected $configObject;
	
	function __construct($configObject) {
		$this->configObject = $configObject;
	}
	
	function getConfigObject() {
		return $this->configObject;
	}
	
	function createAction() {
		Header('X-XSS-Protection: 0');
		$object = new \Cheltar\Model\baseModel($this->configObject);
		
		if (isset($_POST[$this->configObject->getCode()])) {
			$object->setData($_POST[$this->configObject->getCode()]);
			$object->save();
			if (!empty($object->getFirstKey())) {
				$url = \Cheltar\Helper\Factory::getInstance()->getModelApi($this->configObject)->getUrl($object, 'update', 'admin');
				header("location:" . $url);
			}
		}
		
		$form = new \Cheltar\Control\ModelForm($object);
		
		return $form->render();
	}
	
	function updateAction($key) {
		Header('X-XSS-Protection: 0');
		$factory = new \Cheltar\Model\baseModelFactory($this->getConfigObject());
		$object = $factory->getModelByFields(['id' => $key]);
		
		if (isset($_POST[$this->configObject->getCode()])) {
			$object->setData($_POST[$this->configObject->getCode()]);
			$object->save();			
		}
		$apiClassName = $this->configObject->getApiClassName();
		$api = new $apiClassName($this->getConfigObject());
		if (!empty($_SESSION['user'])) { 
			app::getToolsPanel()->addTool(['link' => $api->getUrl($object, 'create', 'admin'), 'text' => 'Добавить']);
			
			if ($this->configObject->getFieldParams('category_id')) {
				app::getToolsPanel()->addTool(['link' => $api->getUrl($object, 'addtosamecategory', 'admin'), 'text' => 'Добавить в эту же категорию']);
			}
			if ($this->configObject->getFieldParams('sef')) {
				app::getToolsPanel()->addTool(['link' => $api->getUrl($object, 'view', 'client'), 'text' => 'Просмотр в клиентской части']);
			}
		}
		$form = new \Cheltar\Control\ModelForm($object);
		
		return $form->render();
	}
	
	function addtosamecategoryAction($key) {
		Header('X-XSS-Protection: 0');
		$factory = new \Cheltar\Model\baseModelFactory($this->getConfigObject());
		$oldObject = $factory->getModelByFields(['id' => $key]);
		$object = new \Cheltar\Model\baseModel($this->configObject);
		$object->category_id = $oldObject->category_id;
		//$categoryId = $object->category_id;
		//$_POST[$this->configObject->getCode()]['category_id'] = $object->category_id;
		if (isset($_POST[$this->configObject->getCode()])) {
			$object->setData($_POST[$this->configObject->getCode()]);
			$object->save();
			if (!empty($object->getFirstKey())) {
				$url = \Cheltar\Helper\Factory::getInstance()->getModelApi($this->configObject)->getUrl($object, 'update', 'admin');
				header("location:" . $url);
			}
		}
		
		$form = new \Cheltar\Control\ModelForm($object);
		
		return $form->render();
	}
			
	function indexAction($filters, $p = 0) {
		
		//todo переделать на методы
		global $db;
		$api = Factory::getInstance()->getModelApi($this->configObject);
		$templateText = \Cheltar\Helper\Template::loadTemplateText("admin/table.php");
		
		$qb = new \Cheltar\Db\queryBuilder();
		$qb->setFrom($this->getConfigObject()->getTableName() . " as main");
				
		$header = ['<!-- -->'];
		$table = [];
		
		$filter = ['captions' => [], 'controls' => []];
		$submit = new \Cheltar\Control\SubmitControl('submit', 'submit');
		$filter['captions'][] = '';
		$filter['controls'][] = $submit->render();

		$controlObjects = [];
		
		foreach ($this->getConfigObject()->getFieldCodesList() as $code) {
			$field = $this->configObject->getFieldParams($code);
			if ($field->getInDefaultTable()) {
				$header[] = $field->getCaption();
				$field->prepareQueryBuilder($qb);
				
				$controlClassName = $field->getDefaultFilterControl();
				$controlObject = new $controlClassName('filter[' . $code . ']', isset($_GET['filter'][$code])?$_GET['filter'][$code]:"");
				if ($controlObject->isNeedOptions()) {
					$controlObject->setOptions($field->getFilterOptions());
				}
				$filter['captions'][] = $field->getCaption();
				$filter['controls'][] = $controlObject->render();
				$controlObjects[$code] = $controlObject;
			}
		}
		if (isset($_GET['filter'])) {
			foreach ($_GET['filter'] as $key => $row) {
				if (strlen($row) > 0) {
					if ($controlObjects[$key]->isNeedOptions()) {
						$qb->addAndWhere("main." . $key . " = '" . addslashes($row) . "'");
					} else {
						$qb->addAndWhere("main." . $key . " LIKE '%" . addslashes($row) . "%'");
					}
				}
			}
		}
		
		$currenPage = isset($_GET['p'])?(int)$_GET['p']:0;
		$countOnPage = 10;
		
		$paginatorObject = (new \Cheltar\Control\Paginator())->setQueryBuilder($qb)
				->setCountOnPage($countOnPage)
				->setUrl($_SERVER['REQUEST_URI']);
				
		$paginator = $paginatorObject->render();

		$qb->setLimit($countOnPage)
				->setOffset($countOnPage * $currenPage);
		
		if ($createField = $this->getConfigObject()->getFieldParams("create_datetime")) {
			$qb->addOrderBy($createField->getTableFieldCode() . " DESC");
		}
		
		$res = $db->getPdo()->query($qb->getQuery(), \PDO::FETCH_ASSOC);
		
		if (!empty($res)) {
			foreach ($res as $row) {
				if (!is_array($row)) {
					continue;
				}
				foreach ($row as $key => $value) {
					$row[$key] = mb_substr(strip_tags($value),0 ,100) ;
				}
				//todo нужны методы для получения ссылок
				$crudLinks = '<a href="/admin/' . $this->configObject->getCode() . '/update/' . $row['id'] . '/">edit</a>';
				$crudLinks .= ' <a href="/admin/' . $this->configObject->getCode() . '/delete/' . $row['id'] . '/" onclick="return confirm(\'Запись будет удалена. Вы уверены?\');">delete</a>';
				array_unshift($row, $crudLinks);
				$table[] = $row;
			}
		}
		$toolsLinks = [];
		if (!empty($_SESSION['user'])) {
			$toolsLinks = [
				['text' => 'Добавить', 'link' => '/admin/' . $this->configObject->getCode() . "/create/"]
			];
		}

		$params = ['header' => $header, 'filter' => $filter, 'table' => $table, 'toolsLinks' => $toolsLinks, 'paginator' => $paginator];
		return \Cheltar\Helper\Template::render($params, $templateText);
		
	}
	
	function viewAction($object) {
		if (!is_object($object)) {
			$factory = new \Cheltar\Model\baseModelFactory($this->getConfigObject());
			$object = $factory->getModelByFields(['id' => $object]);			
		}
		$apiClassName = $this->configObject->getApiClassName();
		$api = new $apiClassName($this->getConfigObject());
		if (!empty($_SESSION['user'])) {
			app::getToolsPanel()->addTool(['link' => $api->getUrl($object, 'update', 'admin'), 'text' => 'Редактировать']);
			if ($this->configObject->getFieldParams('category_id')) {
				app::getToolsPanel()->addTool(['link' => $api->getUrl($object, 'addtosamecategory', 'admin'), 'text' => 'Добавить в эту же категорию']);
			}
			
		}
		$object->view_count++;
		$object->save();
		$templateText = \Cheltar\Helper\Template::loadTemplateText("view.php");		
		//return $object->text;
		return \Cheltar\Helper\Template::render(['text' => $object->text], $templateText);
	}
	
	function deleteAction($key) {
		$factory = new \Cheltar\Model\baseModelFactory($this->getConfigObject());
		$object = $factory->getModelByFields(['id' => $key]);		
		$api = Factory::getInstance()->getModelApi($this->configObject);
		$qb = new \Cheltar\Db\queryBuilder();
		$qb->deleteQuery($this->getConfigObject()->getTableName());
		$kf = $this->getConfigObject()->getKeyFields();
		$qb->addAndWhere($kf[0] . " = " . (int)$key);
		//$qb->setFrom($this->getConfigObject()->getTableName() . " as main");
		app::getPdo()->query($qb->getQuery());
		return "запись успешно удалена. <a href='" . $api->getUrl($object, 'index', 'admin') . "'>вернуться назад</a>";
	}
}
