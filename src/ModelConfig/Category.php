<?php

namespace Cheltar\ModelConfig;

class Category extends BasePageConfig  {

	protected $code = 'category';
	protected $_tableName = 'categories';
	protected $controllerClassName = "\Cheltar\Controller\CategoryController";
	/*	
	function __construct() {
		
		$this->fields['id'] = (new \Cheltar\Field\IntegerField('id', 'Номер'))
				->setKey(true)
				->setDefaultFormControl("\Cheltar\Control\HiddenControl");
		
		$this->fields['title'] = new \Cheltar\Field\StringField('title', "Название");
		
		$this->fields['short_text'] = new \Cheltar\Field\TextField('short_text', "Краткий текст");
		
		$this->fields['text'] = new \Cheltar\Field\TextField('text', "Полный текст");
		
		$this->fields['active'] = new \Cheltar\Field\EnumField('active', 'Включен');
		$this->fields['active']->setOptions([0 => 'нет', 1 => 'да'])
				->setDefaultValue(1);
		
		$this->fields['category_id'] = (new \Cheltar\Field\RelationToOneField('category_id', 'Родительская категория'))
				->setRelatedType('\Cheltar\ModelConfig\Category');
		
		$this->modelClassName = "\Cheltar\Model\baseModel";
	}
	*/
}
