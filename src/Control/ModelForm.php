<?php

namespace Cheltar\Control;

class ModelForm extends Form {

	/**
	 *
	 * @var type \Cheltar\ModelConfig\BaseConfig
	 */
	protected $configObject;
	protected $modelObject;
	
	protected $method = 'POST';
	
	function __construct($modelObject) {
		
		$this->configObject = $modelObject->getModelConfig();
		$this->modelObject = $modelObject;
		$this->name = $this->configObject->getTableName();
	}
	
	function getConfigObject() {
		return $this->configObject;
	}
	
	function render() {
		if (empty($this->fields)) {
			$this->bindDefaultFormFields();
		}
		return parent::render();
	}
	

	function bindDefaultFormFields() {

		foreach ($this->configObject->getFieldCodesList() as $code) {
			$field = $this->configObject->getFieldParams($code);
			$field->setModelObject($this->modelObject);
			if ($field->getInDefaultForm()) {
				$controlClassName = $field->getDefaultFormControl();
				$controlObject = new $controlClassName($this->getControlName($code), $this->modelObject->$code);
				
				$this->bindControl($code, $field->getCaption(), $controlObject);
				
				if ($controlObject->isNeedOptions()) {
					$controlObject->setOptions($field->getOptions());
				}
				$defaultValue = $field->getDefaultValue();
				if (isset($defaultValue)) {
					$controlObject->setDefaultValue($defaultValue);
				}
				
			}
		}
		$this->bindControl('submit', '', new \Cheltar\Control\SubmitControl('submit', ''));
		
	}
	
	function getControlName($code) {
		return $this->configObject->getCode() . '[' . $code . ']';
	}
}