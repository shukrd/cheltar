<?php
namespace Cheltar\Model;

interface RouteNodeInterface {
	
	function __construct($segment, $callback = null, $argumentList = []);
	
	function setParentNode (RouteNodeInterface $parentNode): RouteNodeInterface;
	
	function getParentNode(): RouteNodeInterface;
	
	function addChildNode(RouteNodeInterface $childNode): RouteNodeInterface;
	
	function getChildNode(RouteNodeInterface $segment): RouteNodeInterface;
	
	function getSegment(): string;
	
	function run (array $segmentList);
}