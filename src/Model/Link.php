<?php
namespace Cheltar\Model;
class Link {
	
	protected $params = [];
	protected $baseUrl;
	
	function __construct($url) {
		$this->prepareParams($url);
	}
	
	protected function prepareParams($url) {
		$urlParts = explode("?", $url);
		array_values(array_filter($urlParts));
		$this->baseUrl = $urlParts[0];
		$params = [];
		if (!empty($urlParts[1])) {
			$paramStr = explode("&", $urlParts[1]);
			array_values(array_filter($paramStr));
			foreach ($paramStr as $str) {
				$paramArr = explode("=", $str);
				if (isset($paramArr[1])) {
					$params[$paramArr[0]] = $paramArr[1];
				}
			}
		}
		$this->params = $params;
	}
	
	function addParam($key, $value) {
		$this->params[$key] = $value;
		return $this;
	}
	
	function deleteParam($key) {
		unset($this->params[$key]);
		return $this;
	}

	function setParams($params) {
		$this->params = array_merge($this->params, $params);
		return $this;
	}
	
	function getParams() {
		return $this->params;
	}
	
	function getBaseUrl() {
		return $this->baseUrl;
	}
	
	function getUrl() {
		$arr = [];
		$str = "";
		if (!empty($this->params)) {
			
			foreach ($this->params as $key => $val) {
				$arr[] = "$key=$val";
			}
			$str = "?" . implode("&", $arr);
		}
		
		return $this->baseUrl . $str;
	}
}
?>