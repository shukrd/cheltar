<?php
namespace Cheltar\Control;

class ToolsPanel {
	protected $tools = [];
	protected $templateText = '';
			
	function setTools(array $tools) {
		$this->tools = $tools;
		return $this;
	}
	
	function getTools() {
		return $this->tools;
	}
	
	function addTool($options) {
		//$text, $image, $link
		if (!empty($options['link'])) {
			$this->tools[$options['link']] = $options;
		}
		return $this;
	}
	
	function getTemplateText() {
		if (empty($this->templateText)) {
			$this->templateText = \Cheltar\Helper\Template::loadTemplateText("toolspanel.php");
		}
		
		return $this->templateText;
	}
	
	function setTemplateText($templateText) {
		$this->templateText = $templateText;
		return $this;
	}
	
	function render() {
		//$paginator = $this->getPaginatorArray();
		$assetApi = new \Cheltar\Api\Asset();
		$assetApi->bindSystemStyleFile('/toolspanel.css');
		$templateText = $this->getTemplateText();
		return \Cheltar\Helper\Template::render(['tools' => $this->tools], $templateText);
	}
}
