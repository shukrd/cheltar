<?php if(!empty($items)) {?>
<ul class="<?=$ulClass?>">
<?php foreach($items as $row) {
	$targetHtml = '';
	if (!empty($row['target'])) {
		$targetHtml = "target='_blank'";
	}
	if (!empty($row['update'])) {
		$updateLink = "<div class='menu-li-edit'><a href='" . $row['update'] . "'>edit</a></div>";
	}
	?>
	<li class="<?=$liClass?>"><a href="<?=$row["url"]?>" <?=$targetHtml?>><?=$row['text']?></a></li>
<?php } ?>
</ul>
<?php } ?>
