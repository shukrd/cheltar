<table border=1 cellspacing=0 cellpadding=2 class="calendar"> 
 <tr>
  <td colspan=7> 
   <table width="100%" border=0 cellspacing=0 cellpadding=0> 
    <tr> 
     <td align="right"><a href="<? echo $prev ?>">&lt;&lt;&lt;</a></td> 
     <td align="center"><? echo $month_names[$m-1]," ",$y ?></td> 
     <td align="left"><a href="<? echo $next ?>">&gt;&gt;&gt;</a></td> 
    </tr> 
   </table> 
  </td> 
 </tr> 
 <tr><td>Пн</td><td>Вт</td><td>Ср</td><td>Чт</td><td>Пт</td><td>Сб</td><td>Вс</td><tr>
<? 
$i=0;
//print_r($fill);
for($d=$start;$d<=$end;$d++) {
	$isDateExists = (is_array($fill) AND isset($fill[$d]));
	$isTdEmpty = ($d < 1 OR $d > $day_count);
	$tdClass = 'defaultTd';
	if ($isDateExists) {
		$tdClass = "marked";
	} elseif ($isTdEmpty) {
		$tdClass = "empty";
	}
	if (!($i++ % 7)) echo " <tr>\n";
	echo '  <td align="center" class="' . $tdClass . '">';
	if ($isTdEmpty) {
		echo "&nbsp";
	} else {
		//$now = "$y-$m-".sprintf("%02d",$d);
		if ($isDateExists) {
			echo '<b><a href="' . $fill[$d] . '">'.$d.'</a></b>'; 
		} else {
			echo $d;
		}
    } 
    echo "</td>\n";
    if (!($i % 7))  echo " </tr>\n";
} 
?>
</table> 