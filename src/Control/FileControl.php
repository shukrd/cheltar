<?php

namespace Cheltar\Control;

class FileControl extends BaseControl {
	protected $type = 'file';
	
	
	function render() {
		if (!empty($this->getValue())) {
			$str = $this->getValue() . " <label><input type='checkbox' name='" . $this->getName() . "[delete]' value='1'> удалить</label>";
		} else {
			$str = '';
		}
		return $str . parent::render();
	}
}