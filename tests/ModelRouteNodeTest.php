<?php
use PHPUnit\Framework\TestCase;
use \Cheltar\Model\RouteNode;
class ModelRouteNodeTest extends TestCase
{
	
	
    public function testGetSegment() {
		$node = new RouteNode('test');
		$this->assertEquals($node->getSegment(), 'test');
	}
	
	public function testAddChildGetChild() {
		$node = new RouteNode('');
		$node1 = new RouteNode('test1');
		$node->addChildNode($node1);
		
		$this->assertEquals($node1, $node->getChildNode('test1'));
		
		$this->assertEquals($node1->getParentNode(), $node);
	}
	
	
	
    public function testRun()
    {
		$node = new RouteNode('', function($x) {return $x;}, ['parentNode']);
		$this->assertEquals($node->run([]), 'parentNode');
		$this->assertEquals($node->run(['test4']), 'parentNode'); // несуществующий узел отрабатывает первый из существующих старших узлов

		
		$node->addChildNode(new RouteNode('test1', function($x) {return $x;}, ['Node1']))
			->addChildNode(new RouteNode('test2', function($x) {return $x;}, ['Node2']));
		
		$node->getChildNode('test1')->addChildNode(new RouteNode('test3', function($x) {return $x;}, ['Node3']));
		
		$this->assertEquals($node->run(['test1']), 'Node1');
		$this->assertEquals($node->run(['test2']), 'Node2');
		$this->assertEquals($node->run(['test1', 'test3']), 'Node3');
		$this->assertEquals($node->run(['test1', 'test2']), 'Node1'); // если конечный запрошенный узел не существует, то отрабатывает последний из промежуточных, имеющих калбек, т.е. предполагается, что дальнейшая отработка на промежуточном уровне
		
    }
	

}
?>