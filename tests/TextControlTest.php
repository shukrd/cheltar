<?php
use PHPUnit\Framework\TestCase;

class TextControlTest extends TestCase
{
    public function testControl()
    {
        $object = new \Cheltar\Control\TextControl('test1', 'test2');
	
		$this->assertEquals($object->render(), '<textarea name="test1" class="form-control">test2</textarea>');
	
    }
	

}
?>