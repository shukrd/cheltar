<?php
use PHPUnit\Framework\TestCase;
use Cheltar\Api\BaseApi;
class ApiAssetsTest extends TestCase
{
    public function testGetProjectAssetDir()
    {
        $observer = $this->getMockBuilder("\Cheltar\Api\Asset")
                         ->setMethods(['_getDs', '_getDocumentRoot'])
                         ->getMock();
		
        $observer->expects($this->any())
                 ->method('_getDs')
                 ->will($this->returnValue('/'));
		
        $observer->expects($this->any())
                 ->method('_getDocumentRoot')
                 ->will($this->returnValue('/a/b/c'));
		
		$this->assertEquals($observer->getProjectAssetDir(), '/a/b/c/assets');
    }
	
    public function testGetSystemAssetDir()
    {
        $observer = $this->getMockBuilder("\Cheltar\Api\Asset")
                         ->setMethods(['_getDs', '_getDir'])
                         ->getMock();
		
        $observer->expects($this->any())
                 ->method('_getDs')
                 ->will($this->returnValue('/'));
		
        $observer->expects($this->any())
                 ->method('_getDir')
                 ->will($this->returnValue('/a/b/c'));
		
		$this->assertEquals($observer->getSystemAssetDir(), '/a/b/c/../../assets');
    }
	
	public function testGetDirNameInAsset() {
		$observer = $this->getMockBuilder("\Cheltar\Api\Asset")
                ->setMethods(['_getDs', 'getProjectAssetDir'])
                ->getMock();
		
        $observer->expects($this->any())
				->method('_getDs')
				->will($this->returnValue('/'));
		
        $observer->expects($this->any())
				->method('getProjectAssetDir')
				->will($this->returnValue('/assets'));
		
		$this->assertEquals($observer->getDirNameInAsset("/ab/cd"), '/assets/b1/be/0f02d1e0c6bb2a631a45f21ae2e6');
	}
	public function testBindFile() {
		$observer = $this->getMockBuilder("\Cheltar\Api\Asset")
                ->setMethods(['_getDs', 'getProjectAssetDir', 'getSystemAssetDir', '_copy', '_mkDir', '_fileExists', '_getDocumentRoot'])
                ->getMock();
		
        $observer->expects($this->any())
				->method('_getDs')
				->will($this->returnValue('/'));
		
        $observer->expects($this->any())
				->method('getProjectAssetDir')
				->will($this->returnValue('/project/assets'));

        $observer->expects($this->any())
				->method('_getDocumentRoot')
				->will($this->returnValue('/project'));
		
        $observer->expects($this->any())
				->method('_copy')
				->will($this->returnValue(true))
				->with($this->equalTo('/system/assets/test.css'), $this->equalTo('/project/assets/97/a1/6cea865b23eb5385718b5061a833/test.css'));
		
		$this->assertEquals($observer->bindFile("/system/assets", "test.css"), '/assets/97/a1/6cea865b23eb5385718b5061a833/test.css');


		$observer = $this->getMockBuilder("\Cheltar\Api\Asset")
                ->setMethods(['_getDs', 'getProjectAssetDir', 'getSystemAssetDir', '_copy', '_mkDir', '_fileExists', '_getDocumentRoot'])
                ->getMock();
		
        $observer->expects($this->any())
				->method('_getDs')
				->will($this->returnValue('/'));
		
        $observer->expects($this->any())
				->method('getProjectAssetDir')
				->will($this->returnValue('/project/assets'));

        $observer->expects($this->any())
				->method('_getDocumentRoot')
				->will($this->returnValue('/project'));
		
		
        $observer->expects($this->any())
				->method('_copy')
				->will($this->returnValue(true))
				->with($this->equalTo('/system/assets/t1/test.js'), $this->equalTo('/project/assets/97/a1/6cea865b23eb5385718b5061a833/t1/test.js'));
		
		$this->assertEquals($observer->bindFile("/system/assets", "/t1/test.js"), '/assets/97/a1/6cea865b23eb5385718b5061a833/t1/test.js');
	}
	
	function testPreparePath() {
		$object = $this->getMockBuilder("\Cheltar\Api\Asset")
                ->setMethods(['_getDs'])
                ->getMock();
		
        $object->expects($this->any())
				->method('_getDs')
				->will($this->returnValue('/'));
		
		$this->assertEquals($object->preparePath('//a/b\/d'), '/a/b/d');
		$this->assertEquals($object->preparePath('//a//b/\/d/'), '/a/b/d/');
		
	}

	
	function testGetSrc() {
		$object = $this->getMockBuilder("\Cheltar\Api\Asset")
                ->setMethods(['_getDs', 'getDocumentRoot'])
                ->getMock();
		
        $object->expects($this->any())
				->method('_getDs')
				->will($this->returnValue('/'));
		
        $object->expects($this->any())
				->method('getDocumentRoot')
				->will($this->returnValue('/home/www/'));
		
		$this->assertEquals($object->getSrc('/home/www/ab/123/'), '/ab/123/');
		
	}	
}
?>