<?php

namespace Cheltar\ModelConfig;

class TextBlock extends BaseConfig  {

	protected $code = 'textblocks';
	protected $_tableName = 'textblocks';
	
	function __construct() {
		
		$this->fields['id'] = (new \Cheltar\Field\IntegerField('id', 'Номер'))
			->setKey(true)
			->setDefaultFormControl("\Cheltar\Control\HiddenControl");
		
		$this->fields['title'] = new \Cheltar\Field\StringField('title', "Название");
		
		$this->fields['code'] = new \Cheltar\Field\StringField('code', "Символьный код");
		
		$this->fields['text'] = (new \Cheltar\Field\TextField('text', "Текст без форматирования"))
			->setDefaultFormControl("\Cheltar\Control\TextControl");
		
		$this->fields['formattext'] = new \Cheltar\Field\TextField('formattext', "Текст с форматированием");
		
		$this->fields['active'] = new \Cheltar\Field\EnumField('active', 'Включен');
		$this->fields['active']->setOptions([0 => 'нет', 1 => 'да'])
			->setDefaultValue(1);
		

		
		$this->modelClassName = "\Cheltar\Model\baseModel";
	}
}
