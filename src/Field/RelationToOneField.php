<?php

namespace Cheltar\Field;

class RelationToOneField extends EnumField {
	
	//protected $defaultFormControl = "\Cheltar\Control\StringControl";
	protected $relatedType;
	
	//protected $defaultFormControl = "\Cheltar\Control\SelectControl";
	
	function setRelatedType($relatedType) {
		$this->relatedType = $relatedType;
		return $this;
	}
	
	function getRelatedType() {
		return $this->relatedType;
	}
			
	function getOptions() {
		global $db;
		$code = $this->code;
		$className = $this->relatedType;
		if (class_exists($className)) {
			$configObject = new $className();
			$qb = new \Cheltar\Db\queryBuilder();
			$qb
					//->addAndWhere("active = 1")
					->setFrom($configObject->getTableName())
					->addSelectField('id')
					->addSelectField($code)
					->addSelectField('title');

			$res = $db->getPdo()->query($qb->getQuery(), \PDO::FETCH_ASSOC);
			$arr = [];
			
			if (!empty($res)) {
				foreach ($res as $row) {
					$row['children'] = [];
					$arr[$row['id']] = json_decode(json_encode($row));
				}
			}
			$roots = [];
			$exclude = [];

			
			foreach ($arr as $row) {
				if (isset($arr[$row->$code])) {
					$arr[$row->$code]->children[] = $row;
				} else {
					$roots[] = $row;
				}
			}

			$modelObject = $this->getModelObject();
			if (is_object($modelObject)) {
				if ($modelObject->id) {
					$this->_cascadeDelete($this->getModelObject()->id, $arr);
				}
			}
			$result = [0 => "--верхний уровень--"];
			$this->_parseArr($roots, $result);

			return $result;
		}
		
		//return $this; 
	}
	
	function _cascadeDelete($id, &$arr) {
		if (isset($arr[$id])) {
			if (isset($arr[$id]->children)) {
				foreach ((array)$arr[$id]->children as $row) { 
					$this->_cascadeDelete($row->id, $arr);
				}
			}
			$arr[$id]->exclude = true;
		}
	}
	
	function _parseArr($roots, &$result, $prefix = '') {
		foreach ($roots as $row) {
			if (empty($row->exclude)) {
				$result[$row->id] = $prefix . $row->title;
				if (!empty($row->children)) {
					$this->_parseArr($row->children, $result, $prefix . '-');
				}
			}
		}
	}
	
	function prepareQueryBuilder($qb) {
		$className = $this->relatedType;
		if (class_exists($className)) {
			$configObject = new $className();
			
			$qb->addSelectField("t1.title as " . $this->code);
			$qb->addJoin("LEFT JOIN " . $configObject->getTableName() . " as t1 ON t1.id = main." . $this->getCode() );
		}
	}
	
}
