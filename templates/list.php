<div class="list-text">
<?=$text?>
</div>
<div class="list-items">
<?php 
if (!empty($list)) foreach($list as $item) { ?>
	<div class="list-item">
		<div class="list-item-title"><a href="<?=$item['link']?>"><?=$item['title']?></a></div>
<?php
if (!empty($item['create_datetime'])) {
?>
		<div class="list-item-datetime"><?=$item['create_datetime']?></div>
<?php
}
?>
		
<?php
if (!empty($item['short_text'])) {
?>
<? if (!empty($item['list_image'])) {
?>
	<div class="list-item-image"><img style="" src="<?=$item['list_image']?>"></div>
<?
}
?>
		<div class="list-item-short-text">

		<?=$item['short_text']?>
		
		</div>
	<br style="clear: both;">	
<?php
}
?>
	</div>
<?php } ?>
</div>

<div>
	
<?php
if (!empty($paginator)) {
 echo $paginator;
}
?>
</div>
