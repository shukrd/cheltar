<?php
use PHPUnit\Framework\TestCase;
use Cheltar\Helper\Template;
class HelperTemplateTest extends TestCase
{
    public function testRender()
    {
		$this->assertEquals(
			Template::render(
				['f1' => 'ab', 'f2' => 'ac'],
				'echo "qwe " . $f1 . " rty " . $f2 . " uio  p";'
			),
			'qwe ab rty ac uio  p'
		);
    }
	
	public function test_GetModuleDir() {
		$this->assertEquals(
			Template::_getModuleDir(DIRECTORY_SEPARATOR . "abc"),
			DIRECTORY_SEPARATOR . "abc" . DIRECTORY_SEPARATOR . ".." . DIRECTORY_SEPARATOR . ".." . DIRECTORY_SEPARATOR
		);
	}
	

}
?>